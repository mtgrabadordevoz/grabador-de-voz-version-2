package com.mercadeotodo.grabadordevoz.Clases;

import java.util.Comparator;

/**
 * Created by Jose Montero on 08/01/2016.
 */
public class MyComparator implements Comparator<Audio> {
    @Override
    public int compare(Audio p1, Audio p2) {
        return (int) p1.getSIZE_NUMERIC()- (int) p2.getSIZE_NUMERIC();
    }}
