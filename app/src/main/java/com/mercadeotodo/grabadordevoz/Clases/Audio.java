package com.mercadeotodo.grabadordevoz.Clases;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Jose Montero on 08/12/2015.
 */
public class Audio implements Parcelable {
    private String ID, SIZE, TITLE, DATA, NAME, DURATION, DATE;
    private double SIZE_NUMERIC;

    public Audio(String ID, String SIZE, String TITLE, String DATA, String NAME, String DURATION, String DATE, double SIZE_NUMERIC) {
        this.ID = ID;
        this.SIZE = SIZE;
        this.TITLE = TITLE;
        this.DATA = DATA;
        this.NAME = NAME;
        this.DURATION = DURATION;
        this.DATE = DATE;
        this.SIZE_NUMERIC = SIZE_NUMERIC;
    }

    //<editor-fold desc="Metodos Get">
    public String getID() {
        return ID;
    }

    public String getSIZE() {
        return SIZE;
    }

    public String getTITLE() {
        return TITLE;
    }

    public String getDATA() {
        return DATA;
    }

    public String getNAME() {
        return NAME;
    }

    public String getDURATION() {
        return DURATION;
    }

    public String getDATE() {
        return DATE;
    }

    public double getSIZE_NUMERIC() {
        return SIZE_NUMERIC;
    }

    //</editor-fold>
    //<editor-fold desc="Metodos Set">
    public void setID(String ID) {
        this.ID = ID;
    }

    public void setSIZE(String SIZE) {
        this.SIZE = SIZE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public void setDURATION(String DURATION) {
        this.DURATION = DURATION;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public void setSIZE_NUMERIC(double SIZE_NUMERIC) {
        this.SIZE_NUMERIC = SIZE_NUMERIC;
    }
    //</editor-fold>

    protected Audio(Parcel in) {
        ID = in.readString();
        SIZE = in.readString();
        TITLE = in.readString();
        DATA = in.readString();
        NAME = in.readString();
        DURATION = in.readString();
        DATE = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(SIZE);
        dest.writeString(TITLE);
        dest.writeString(DATA);
        dest.writeString(NAME);
        dest.writeString(DURATION);
        dest.writeString(DATE);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Audio> CREATOR = new Parcelable.Creator<Audio>() {
        @Override
        public Audio createFromParcel(Parcel in) {
            return new Audio(in);
        }

        @Override
        public Audio[] newArray(int size) {
            return new Audio[size];
        }
    };

    /*Comparator para realizar la ordenacion de las listas, notese que estas clases por si solas no ordenan
    sino que son utilizadas por el metodo Collection sort, en el recycler view adapter, estas solo comparan*/
    public static class ComparatorSize implements Comparator<Audio> {
        @Override
        public int compare(Audio arg0, Audio arg1) {
            /*utilizamos la clase comparator para ir ordenando los tamaños en forma acendente,
            * ojo aqui se comparan unicamente 2 audios*/
            return (int) arg0.getSIZE_NUMERIC() - (int) arg1.getSIZE_NUMERIC();
        }
    }

    public static class ComparatorName implements Comparator<Audio> {

        @Override
        public int compare(Audio arg0, Audio arg1) {
            /*utilizamos el comparator para ordenar los audios por nombre, ojo aqui solo se comparan 2 audios,
            * y se ignora las mayusculas y minusculas*/
            return arg0.getNAME().compareToIgnoreCase(arg1.getNAME());
        }
    }
    public static class ComparatorDate implements Comparator<Audio> {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm ");

        @Override
        public int compare(Audio arg0, Audio arg1) {
            /*utilizamos el comparator para ordenar los audios por nombre, ojo aqui solo se comparan 2 audios,
            * y se ignora las mayusculas y minusculas*/
            try {
                Date date1 = formatter.parse(arg0.getDATE());
                Date date2 = formatter.parse(arg1.getDATE());
                return date1.compareTo(date2);
            } catch (ParseException e) {
                e.printStackTrace();
                return arg0.getDATE().compareTo(arg1.getDATE());
            }

        }
    }
}
