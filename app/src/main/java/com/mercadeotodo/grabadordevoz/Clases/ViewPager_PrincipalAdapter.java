package com.mercadeotodo.grabadordevoz.Clases;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mercadeotodo.grabadordevoz.Fragment.AjustesFragment;
import com.mercadeotodo.grabadordevoz.Fragment.GrabacionFragment;
import com.mercadeotodo.grabadordevoz.Fragment.ListadoAudioFragment;
import com.mercadeotodo.grabadordevoz.Fragment.ReproductorFragment;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

import java.util.ArrayList;

/**
 * Created by Jose Montero on 08/12/2015.
 */
public class ViewPager_PrincipalAdapter extends FragmentStatePagerAdapter {
    private int[] imageResId = {
            R.mipmap.ic_record_voice_over_black_24dp,
            R.mipmap.ic_audiotrack_black_24dp,
            R.mipmap.ic_settings_black_24dp
    };
    private Context context;
    private ArrayList<Audio> listado;
    private Comunicacion mListener;
    public ViewPager_PrincipalAdapter(FragmentManager fm, Context context,ArrayList<Audio> listado) {
        super(fm);
        this.context = context;
        this.listado = listado;
        this.mListener = (Comunicacion) context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:{
                return GrabacionFragment.newInstance();
            }
            case 1:{
                return ListadoAudioFragment.newInstance(listado);
            }
            case 2:{
                /*obtengo una referencia al audio seleccionado, por defecto esta nulo*/
                return ReproductorFragment.newInstance(mListener.getAudioSeleccionadoParaFragmentReproductor());
            }
            case 3: {
                return AjustesFragment.newInstance();
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }


    public void setListadoDeAudio(ArrayList<Audio> listado){
        this.listado = listado;
    }
}
