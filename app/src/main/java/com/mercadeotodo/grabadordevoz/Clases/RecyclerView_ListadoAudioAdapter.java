package com.mercadeotodo.grabadordevoz.Clases;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mercadeotodo.grabadordevoz.Fragment.PrincipalFragment;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Jose Montero on 08/12/2015.
 */
public class RecyclerView_ListadoAudioAdapter extends RecyclerView.Adapter<RecyclerView_ListadoAudioAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private FragmentActivity contexto;
    private MyViewHolder myViewHolder;
    private Comunicacion mListener;
    List<Audio> audios = Collections.emptyList();
    List<Audio> audiosRespaldo = Collections.emptyList();
    List<Audio> audiosParaBorrar = Collections.emptyList();
    private MyAsynkTask myAsynkTask;
    /*parametros utilizados para el renombramiento de audios*/
    private Audio nuevoAudio;
    private Audio audioOriginal;
    private String textoBotonPositivo = "";
    PrincipalFragment principalFragment;

    public RecyclerView_ListadoAudioAdapter(FragmentActivity contexto, List<Audio> listado) {
        this.contexto = contexto;
        inflater = LayoutInflater.from(contexto);
        this.contexto = contexto;
        this.audios = listado;
        this.audiosRespaldo = listado;
        this.mListener = (Comunicacion) contexto;
        this.nuevoAudio = null;
        this.audioOriginal = null;
        audiosParaBorrar = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         /*asociamos un view y lo inflamos con el xml del modelo de lista que tengamos*/
        View view = inflater.inflate(R.layout.modelo_listado_audios, parent, false);
        /*luego instanciamos un objeto MyviewHolder pasando por parametro el view*/
        myViewHolder = new MyViewHolder(view);
        /*retornamos el viewholder creado, para el proceso de reciclaje del recycle view*/

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        /*este metodo es llamado siempre que se hace scroll en el recycler view
        aqui vamos actualizando uno a uno todos los elementos que se encuentran en el recycler*/
        Audio audioActual = audios.get(position);
        holder.textTamanoAudio.setText(audioActual.getSIZE());
        holder.textDuracionAudio.setText(audioActual.getDURATION());
        holder.textFechaAudio.setText(audioActual.getDATE());
        holder.textNombreAudio.setText(audioActual.getNAME());
        /*verificamos si el audio actual se encuentra en la lista de audios para borrar y actualizamos
        el checkbox que corresponde a este audio, esto se lleva a cabo para evitar conflictos cuando
        el recycler view lleve a cabo el proceso de reciclado de componentes*/
        if (audiosParaBorrar.contains(audioActual)){
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        try {
            return audios.size();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    public void ordenarArchivos(String opcion){
        Log.e("ordenarArchivos", "ORDENAR ARCHIVOS");
        switch (opcion){
            case "Name": {
                /*utilizamos el metodo Collections.sort pasando como parametro la lista de audios y el
                metodo de ordenacion el cual se encuentra en la clase audio*/
                Collections.sort(audios, new Audio.ComparatorName());
                break;
            }
            case "Size": {
                Collections.sort(audios, Collections.reverseOrder(new Audio.ComparatorSize()));
                break;
            }
            case "Date":{
                Collections.sort(audios, Collections.reverseOrder(new Audio.ComparatorDate()));
                break;
            }
        }
        /*luego de ordenar las listas, limpiamos y actualizamos la lista de respaldos que utilizamos aqui,
        y la lista de nombres utilizadas para obtener la posicion de un audio en el listado de audios en el main activity*/
        mListener.getListadoDeNombresDeAudios().clear();
        audiosRespaldo = audios;
        for (int i = 0; i<audios.size();i++){
            mListener.getListadoDeNombresDeAudios().add(audios.get(i).getNAME());

        }

    }
    public void iniciarORdenacion(String opcion){
        /*mandamos a configurar el texto para agregar el nombre de todos los audios que van a ser eliminados,
        esto lo hacemos en un nuevo hilo de ejecucion*/
        new MyAsynkTask(opcion).execute();
    }
    public List<Audio> BuscarAudio(String charText) {
        /*metodo encargado de crear un List filtrado con el contenido que esta buscando el usuario unicamente*/
        Log.e("Filter", "Voy a Filtrar: " + charText + " " + charText.length());
        /*pasamos a minuscula  el string recibido por parametro*/
        charText = charText.toLowerCase();
        /*posteriormente creamos un nuevo List que sirva como lista filtrada*/
        final List<Audio> filteredModelList = new ArrayList<>();
        /*si el contenido recibido por parametro es 0, a la nueva lista filtrada le asignamos
        el contenido completo del list audiosRespaldo el cual contiene todos los audios registrados*/
        if (charText.length() == 0){
            filteredModelList.addAll(audiosRespaldo);
        } else {
            /*en caso contrario recorremos el list audiosRespaldo para buscar un match de lo recibido por parametro*/
            for (Audio model : audiosRespaldo) {
                final String text = model.getNAME().toLowerCase();
                if (text.contains(charText)) {
                    /*si existe un audio que contenga algo del string recibido por parametro, agregamos ese audio
                    a la lista filtrada*/
                    filteredModelList.add(model);
                }
            }
        }
        /*finalmente devolvemos la lista filtrada*/
        return filteredModelList;

    }
    private void getPrincipalFragment (){
        FragmentManager manager = contexto.getSupportFragmentManager();
        principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");
    }

    public void deleteSong(String path){
       /*creamos un archivo a partir de la ruta recibida por parametro*/
        final File archivo = new File(path);
    /*procedemos a eliminar la informacion del media store del audio al que pertenece la ruta recibida por parametro*/
        try {
            MediaScannerConnection.scanFile(contexto, new String[]{path},
                    null, new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                            contexto.getContentResolver()
                                    .delete(uri, null, null);
                            /*si pasaron true por el parametro booleano deleteFile, eliminamos el archivo de la sd
                            * esto para diferenciar de cuando se renombra un archivo y de cuando se desea eliminar un archivo*/
                                archivo.delete();


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void iniciarVaciadoDeAudios(){
        /*mandamos a configurar el texto para agregar el nombre de todos los audios que van a ser eliminados,
        esto lo hacemos en un nuevo hilo de ejecucion*/
        new MyAsynkTask("TextoEliminar").execute();
    }
    public void showDeleteSongDialog(){
        /*metodo que muestra un alert dialog para que el usuario confirme si desea
               * eliminar los audios seleccionados */
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(contexto);
        alertBuilder.setTitle(contexto.getString(R.string.DeleteAlertDialog));
        alertBuilder.setMessage(textoBotonPositivo);
        /*se configura el alertDialogBuilder*/
        alertBuilder.setPositiveButton(contexto.getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*arrancamos un segundo hilo de ejecucion para eliminar la informacion solicitada*/
                new MyAsynkTask("Delete").execute();
                getPrincipalFragment();
                principalFragment.ocultarIconoBorrarAudio();

            }
        });

        alertBuilder.setNegativeButton(contexto.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        /*se instancia ahora un AlertDialog partiendo del AlertDialogBuilder*/
        final AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }
    public void configurarText(){
        /*metodo para configurar el listado de audios que piensan ser eliminados*/
        if (audiosParaBorrar.size() > 0){
            textoBotonPositivo = "";
            for (int i = 0;i< audiosParaBorrar.size();i++){
                textoBotonPositivo = textoBotonPositivo+"\n"+audiosParaBorrar.get(i).getNAME()+"\n";
            }

        }
    }
    public void iniciarEdicionDeAudio(Audio audio, Audio audioNuevo){
        /*salvamos el audio original y el nuevo, para poder encontrar el audio en la lista y cambiar su informacion
        por la nueva*/
        audioOriginal = audio;
        nuevoAudio = audioNuevo;
        /*mandamos a acutualizar la informacion de las listas que se encuentran en el main activity*/
        mListener.changeAudioArrayList(audioOriginal,nuevoAudio);
        /*arrancamos un segundo hilo de ejecucion para editar los audios recibidos por parametros*/
        new MyAsynkTask("Rename").execute();
    }
    public void renombrarAudio(){
        /*recorremos el arreglo de audios y buscamos la direccion del audio que fue cambiado por el usuario
        para cambiar la informacion*/
       for (int i = 0; i<audios.size();i++){
           if (audios.get(i).getDATA().equals(audioOriginal.getDATA())){
               audios.set(i,nuevoAudio);
               notifyItemChanged(i);
               return;
           }
       }
    }
    public void limpiarRecyclerView(Audio audio){
        /*metodo utilizado para eliminar un audio del recycler view,
        en primer lugar verificamos si la lista utilizada para manejar el recycler view
        contiene a ese audio, y en caso de contenerlo lo eliminamos de la lista*/
        if (audios.contains(audio)){
            audios.remove(audio);
        }

    }
    public void eliminarAudios(){
        /*funcion principal para eliminar audios, aqui recorremos toda la lista de audios para borrar
        y llamamos al metodo para eliminar el item del recycler view y para borrar el audio de la memoria*/
        Log.e("Eliminar audios","eliminar audios recycler view");
        for (int i = 0; i< audiosParaBorrar.size();i++){
            limpiarRecyclerView(audiosParaBorrar.get(i));
            deleteSong(audiosParaBorrar.get(i).getDATA());
            mListener.removeAudioArrayList(audiosParaBorrar.get(i));
        }
    }
    public void eliminarAudiosDeFormaForzada(Audio audioAEliminar){
        /*esta funcion es utilizada para eliminar un audio desde el fragment de reproduccion,
        aqui agregamos el audio a eliminar a la lista y posteriormente iniciamos el procecso
        de limpieza de manera regular a traves de un asyntask*/
        for (int i = 0;i< audios.size();i++){
            if (audios.get(i).getDATA().equals(audioAEliminar.getDATA())){
                audiosParaBorrar.add(audios.get(i));
                Log.e("ENCONTRE UN MATCH: ",audios.get(i).getDATA()+" "+audioAEliminar.getDATA());
                /*arrancamos un segundo hilo de ejecucion para eliminar la informacion solicitada*/
                new MyAsynkTask("Delete").execute();
                return;
            }
        }

    }
    public void setListadoAudio(List<Audio> listadoAudio){
        /*metodo encargado de asignar la nueva lista que debe mostrar el recycler view
        esta nueva lista se recibe por parametro y se le asigna al list audios, se le asigna a audios
        por que es esa la lista utilizada para armar el contenido del recycler view*/
        this.audios = listadoAudio;
        /*llamamos a notifyDataSerChanged para actualizar el recyclerview*/
        notifyDataSetChanged();
    }
    public void addAudio(Audio audio){
        /*recibimos el nuevo audio por parametro, lo agregamos a la lista
        utilizada para mostrar la informacion y notificamos el cambio para que se actualize*/
        //this.audios.add(audio);
        /*llamamos al metodo agregar audio de la interfaz para registrar el nuevo audio en los arrays*/
        mListener.agregarAudio(audio);
        notifyDataSetChanged();

    }
    public List<Audio> getAudios(){
        /*metodo para retornar la lista de audios registrados en el sistema,
        * utilizamos la lista audiosRespaldo por que es la que nunca es modificada
        * y siempre mantiene todos los audios registrados en el sistema*/
        return this.audiosRespaldo;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textNombreAudio, textDuracionAudio, textTamanoAudio,textFechaAudio;
        CheckBox checkBox;
        RelativeLayout relativeLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            textDuracionAudio = (TextView) itemView.findViewById(R.id.modelo_listado_audios_duracion);
            textNombreAudio = (TextView) itemView.findViewById(R.id.modelo_listado_audios_nombre);
            textFechaAudio = (TextView) itemView.findViewById(R.id.modelo_listado_audios_fecha);
            textTamanoAudio = (TextView) itemView.findViewById(R.id.modelo_listado_audios_tamano);
            checkBox = (CheckBox) itemView.findViewById(R.id.modelo_listado_audios_checkbox);
            relativeLayout =(RelativeLayout) itemView.findViewById(R.id.modelo_listado_audios_RelativeLayout);
            relativeLayout.setOnClickListener(this);
            relativeLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            RelativeLayout view = (RelativeLayout) v;
                            view.setBackgroundColor(contexto.getResources().getColor(R.color.Cyan600));
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            RelativeLayout view = (RelativeLayout) v;
                            view.setBackgroundColor(contexto.getResources().getColor(R.color.Grey300));
                            break;
                        }
                        case MotionEvent.ACTION_MOVE: {
                            RelativeLayout view = (RelativeLayout) v;
                            view.setBackgroundColor(contexto.getResources().getColor(R.color.Grey300));
                            break;
                        }
                    }
                    return false;
                }
            });
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /*colocamos un listener al checkBox para saber cuando el usuario selecciona un audio*/
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        /*si el checkbox es activado, verificamos primero si ese audio se encuentra registrado en la lista
                        de audios para borrar y de no estarlo se agrega*/
                        if (!audiosParaBorrar.contains(audios.get(getPosition()))) {
                            audiosParaBorrar.add(audios.get(getPosition()));

                        }
                        /*posteriormente obtenemos una referencia al fragment principal y mandamos a mostrar el icono de borrado*/
                        FragmentManager manager = contexto.getSupportFragmentManager();
                        PrincipalFragment principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");
                        if (principalFragment != null){
                            principalFragment.mostrarIconoBorrarAudio();
                        }
                    } else {
                        /*en caso de que el checkbox se desmarque, verificamos si ese audio se encuentra en
                        la lista de audios para borrar y lo eliminamos de la lista*/
                        if (audiosParaBorrar.contains(audios.get(getPosition()))){
                            audiosParaBorrar.remove(audios.get(getPosition()));
                        }
                        if (audiosParaBorrar.size() == 0){
                            /*si el listado de audios para borrar es igual a 0 significa que no hay ningun audio marcado
                            por consiguiente obtenemos una referencia del fragment principal y mandamos a ocultar el icono
                            de borrar*/
                            FragmentManager manager = contexto.getSupportFragmentManager();
                            PrincipalFragment principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");
                            if (principalFragment != null){
                                principalFragment.ocultarIconoBorrarAudio();
                            }
                        }
                    }

                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.modelo_listado_audios_RelativeLayout:{
                    mListener.setAudioSeleccionado(textNombreAudio.getText().toString());
                    mListener.cambiarPantalla(contexto.getString(R.string.CambiarPAntallaReproductor));
                    break;
                }
            }
        }
    }

    private class MyAsynkTask extends AsyncTask <String, Void, Boolean> {
        String opcion;

        public MyAsynkTask(String opcion) {
            this.opcion = opcion;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            switch (opcion){
                case "Delete":{
                    eliminarAudios();
                    break;
                }
                case "Rename":{
                    renombrarAudio();
                    break;
                }
                case "TextoEliminar":{
                    configurarText();
                    break;
                }
            }
            if ((opcion.equals("Name")) || (opcion.equals("Size")) || (opcion.equals("Date"))  ){
                /*si la opcion es para ordenar "Name" o "Size", se llama al metodo ordenarArchivos
                pasando la opcion como parametro, en dicho metodo tmb se valida esa opcion*/
                ordenarArchivos(opcion);
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
             /*llamamos a notifyDataSetchanged para actualizar el recycler view*/
            switch (opcion){
                case "TextoEliminar":{
                    /*mostramos el alert dialog para esperar la confirmacion del usuario para eliminar las grabaciones*/
                    showDeleteSongDialog();
                    break;
                }
                case "Delete":{
                    /*vaciamos la lista de audios para borrar*/
                    audiosParaBorrar.clear();
                    break;
                }
            }
            notifyDataSetChanged();
        }
    }

}

