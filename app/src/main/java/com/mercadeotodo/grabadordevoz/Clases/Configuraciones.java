package com.mercadeotodo.grabadordevoz.Clases;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Environment;

/**
 * Created by Jose Montero on 07/12/2015.
 */
public class Configuraciones {
    private String ruta =  Environment.getExternalStorageDirectory().getPath() + "/GrabadorDeVozPlus/";

    public String getRuta() {
        return ruta;
    }
    //<editor-fold desc="Save SharedPreferences">
    public void saveStartedTime(Activity context, long time){
        /*almacenamiento en un shared preference el tiempo cuando se inicio la grabacion,
        esto para cuando la aplicacion sea minimizada y abierta de nuevo cuadrar el cronometro
        para mantener el tiempo que lleva grabando*/
        SharedPreferences sharedPreferences = context.getSharedPreferences("Time", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putLong("startedtime", time);
        editor.commit();
    }
    public void savePauseTime(Activity context, long time){
        /*almacenamiento en un shared preference del tiempo exacto cuando la grabacion fue pausada
        * esto para mantener el orden cuando el usuario continue la grabacion*/
        SharedPreferences sharedPreferences = context.getSharedPreferences("PauseTime", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putLong("pausestime", time);
        // edit.putString("password", txtPass.getText().toString().trim());
        editor.commit();
    }
    public void saveRecordingRoute(Activity context, String route){
        /*almacenamiento en un shared preference de la nueva ruta de grabacion*/
        SharedPreferences sharedPreferences = context.getSharedPreferences("RecordingRoute", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putString("recordingroute", route);
        // edit.putString("password", txtPass.getText().toString().trim());
        editor.commit();
    }
    //</editor-fold>

    //<editor-fold desc="Get Shared Preferences">
    public String getRecordingRoute(Activity context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("RecordingRoute", 0);
        return sharedPreferences.getString("recordingroute", this.ruta);
    }
    public long getStartedTime(Activity context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Time", 0);
        return sharedPreferences.getLong("startedtime",0);
    }
    public long getPauseTime(Activity context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("PauseTime", 0);
        return sharedPreferences.getLong("pausestime",0);
    }
    //</editor-fold>
}
