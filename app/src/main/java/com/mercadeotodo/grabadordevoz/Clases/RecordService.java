package com.mercadeotodo.grabadordevoz.Clases;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import com.mercadeotodo.grabadordevoz.R;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Jose Montero on 18/12/2015.
 */
public class RecordService extends Service {
    private final IBinder mBinder = new LocalBinder();

    public static final int NUM_CHANNELS = 1;
    public static final int SAMPLE_RATE = 16000;
    public  final int BITRATE =  100;
    public static final int MODE = 1;
    private static final int QUALITY = 2;
    private String audioName = "";
    public static final String EXTRA_KEY_IN = "EXTRA_IN";
    public final static String MY_ACTION = "MY_ACTION";
    private short[] mBuffer;
    private AudioRecord mRecorder;
    private boolean mIsRecording;
    private File mEncodedFile;
    private File mRawFile;
    private boolean flag;
    private boolean isEncodingFile;
    private Activity activity;
    private boolean isPauseBottonPresse;


    // public static final int QUALITY = 1;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("onBind","Voy a Bind Service");
        try {
            audioName = intent.getStringExtra(EXTRA_KEY_IN);
            inicializarLameMethods();
            iniciarGrabacion();
            //mergeAudio();
        } catch (Exception e) {
            Log.d("Start", "onERROR EN on STart ");
            e.printStackTrace();
        }
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isEncodingFile = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //updateWidget();
        return super.onStartCommand(intent, flags, startId);
    }
    private void inicializarLameMethods() {
        /*metodo que inicializa e instancia los metodos nativos de Lame Encoder
        para codificar las grabaciones y transformarlas en mp3*/
        Log.e("iniciarlizarLameMEtho","Inicializar LAmes");
        initRecorder();
        initEncoder(NUM_CHANNELS, SAMPLE_RATE, BITRATE, MODE, QUALITY);
    }
    private void initRecorder() {
        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        mBuffer = new short[bufferSize];
        mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize);
    }
    public void iniciarGrabacion(){
        Log.e("iniciarlizarGrabacion","Inicializar LAmes");
        /*obtener el espacio disponible en la tarjeta sd*/
      /*  StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long bytesAvailable = (long)stat.getBlockSizeLong() * (long)stat.getAvailableBlocksLong();
        System.out.println("Bytes: "+bytesAvailable);*/
        mRecorder.startRecording();
        mRawFile = getFile("raw");
        mIsRecording = true;
        startBufferedWrite(mRawFile);
        Toast.makeText(this, getApplicationContext().getString(R.string.Grabando), Toast.LENGTH_SHORT).show();
    }
    public void stopRecording(){
        flag = true;
        isEncodingFile = true;
        /*cuando se mande a detener o finalizar el servicio se llama al metodo finalizarGrabacion
            para cerrar la grabacion*/
        try {

            Log.d("Destroy", "onDestroy");
            finalizarGrabacion();
            mRecorder.release();
            //destroyEncoder();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Destroy", "ERROR onDestroy PersonalIntentService Java");
            finalizarGrabacion();
            mRecorder.release();
        }
    }
    public void finalizarGrabacion(){
        Log.d("finalizarGrabacion", this.audioName);
        mIsRecording = false;
        mRecorder.stop();
        mEncodedFile = getFile("mp3");
        new MyAsyncTask().execute("");
    }
    public void pauseRecording(){
        /*cuando se presione pause, se para el record*/
        Toast.makeText(this, getApplicationContext().getString(R.string.GrabacionPausada), Toast.LENGTH_SHORT).show();
        isPauseBottonPresse = true;
        mIsRecording = false;
        mRecorder.stop();
    }
    public void reasumeRecording(){
        /*cuando se presione play de nuevo, se reasume la grabacion
        y se pone en false la variable isPauseBottonPResse*/
        isPauseBottonPresse = false;
        iniciarGrabacion();
    }

    public boolean isPauseBottonPresse() {
        /*chequeo de si se presiono el boton pause o no*/
        return isPauseBottonPresse;
    }
    public boolean isEncodingFile() {
        /*se verifica si el servicio esta codificando el nuevo audio*/
        return isEncodingFile;
    }
    private File getFile(final String suffix) {
        Time time = new Time();
        time.setToNow();
        return new File(this.audioName + "." + suffix);
    }
    private void startBufferedWrite(final File file) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean broadcastFlagWarning = false;
                boolean broadcastFlag = false;
                DataOutputStream output = null;
                try {
                    output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file,true)));
                    while (mIsRecording) {
                        int readSize = mRecorder.read(mBuffer, 0, mBuffer.length);
                        for (int i = 0; i < readSize; i++) {
                            output.writeShort(mBuffer[i]);
                        }
                        // Log.e("startBufferedWrite","Size: "+file.length());
                    }
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } finally {
                    if (output != null) {
                        try {
                            output.flush();
                        } catch (IOException e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        } finally {
                            try {
                                output.close();
                            } catch (IOException e) {
                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        }).start();
    }

    // load the library - name matches jni/Android.mk
    static {
        System.loadLibrary("mp3lame");
    }

    // declare the native code function - must match wraper.c
    private native void initEncoder(int numChannels, int sampleRate, int bitRate, int mode, int quality);
    private native void destroyEncoder();
    private native int encodeFile(String sourcePath, String targetPath);


    public class LocalBinder extends Binder {
        public RecordService getService() {
            // Return this instance of LocalService so clients can call public methods
            return RecordService.this;
        }
    }
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public class MyAsyncTask extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            int result = encodeFile(mRawFile.getAbsolutePath(), mEncodedFile.getAbsolutePath());
            return String.valueOf(result);
        }

        @Override
        protected void onPostExecute(String s) {
            int result = Integer.parseInt(s);
            if (result == 0) {
                try {
                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.ArchivoGuardado) + " "+mEncodedFile.getName(), Toast.LENGTH_SHORT)
                            .show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                File archivo = new File(audioName+".raw");
                archivo.delete();
                destroyEncoder();
             /*registramos en el media scanner los datos del nuevo audio grabado*/
                MediaScannerConnection.scanFile(getApplicationContext(),
                        new String[]{audioName + ".mp3"}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                                /*mandamos broadcast para actualizar la pantalla record*/
                                Intent intent = new Intent();
                                intent.putExtra("Opcion", 0);
                                intent.putExtra("AudioFile", audioName);
                                intent.setAction(MY_ACTION);
                                sendBroadcast(intent);

                            }
                        });
            }

        }
    }

}
