package com.mercadeotodo.grabadordevoz.Interfaz;

import android.content.Intent;

import com.mercadeotodo.grabadordevoz.Clases.Audio;
import com.mercadeotodo.grabadordevoz.Clases.RecordService;

import java.util.ArrayList;

/**
 * Created by Jose Montero on 07/12/2015.
 */
public interface Comunicacion {
    public RecordService getService();
    public void ListenerBindService(Intent intent);
    public void ListenerUnbindService();
    public void cambiarPantalla(String opcion);
    public void openFilePicker(int opcion, String opc);
    public String getAudioSeleccionado();
    public void setAudioSeleccionado(String audio);
    public void changeAudioArrayList(Audio audioOriginal, Audio audioNuevo);
    public void removeAudioArrayList(Audio audioOriginal);
    public void agregarAudio(Audio nuevoAudio);
    public boolean readableFlag();
    public boolean isMyServiceRuning(Class<?> serviceClass);
    public ArrayList<Audio> getListadoAudios();
    public boolean returnInterstecialOpened();
    public void mostrarInterstecial();
    public Audio getAudioSeleccionadoParaFragmentReproductor();
    public ArrayList<String> getListadoDeNombresDeAudios();
    public void ordenarArchivos(String opcion);
}
