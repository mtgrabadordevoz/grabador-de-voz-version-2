package com.mercadeotodo.grabadordevoz.Actividad;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.mercadeotodo.grabadordevoz.Clases.Audio;
import com.mercadeotodo.grabadordevoz.Clases.Configuraciones;
import com.mercadeotodo.grabadordevoz.Clases.MyApplication;
import com.mercadeotodo.grabadordevoz.Clases.RecordService;
import com.mercadeotodo.grabadordevoz.Fragment.AjustesFragment;
import com.mercadeotodo.grabadordevoz.Fragment.InformacionFragment;
import com.mercadeotodo.grabadordevoz.Fragment.PrincipalFragment;
import com.mercadeotodo.grabadordevoz.Fragment.ReproductorFragment;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.splunk.mint.Mint;

import java.io.File;
import java.io.FilenameFilter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements Comunicacion {
    private Configuraciones configuraciones;
    private ArrayList<Audio> arrayList;
    private ArrayList<String> audios;
    private String audio;
    ReproductorFragment reproductorFragment;
    private String opcion;/*variable para almacenar distintas opciones emitidas por otros fragments para copiar, mover audios*/
    private RecordService mService;
    private boolean sdWritable;
    private boolean mBound = false;
    MyReceiver myReceiver;
    private String audioFile;
    private PrincipalFragment principalFragment;
    private Audio audioTape; /*audioTape guardara cada grabacion nueva que sea creada*/
    private int screenNumber = 0; /*variable para verificar en que pantalla nos encontramos y colocar la actividad en un backstack*/
    private InterstitialAd interstitial;
    private boolean advertisingLoad = false;
    public boolean intersticial;
    private Audio audioSeleccionado;
    private int read_permission = 0;
    private int write_permission = 0;
    private int record_permission = 0;
    List<String> permissions = new ArrayList<String>();

    //<editor-fold desc="Metodos SobreEscritos">


    @Override
    protected void onResume() {
        super.onResume();
        /*gogle analytucs*/
        MyApplication.getInstance().trackScreenView("Actividad Principal");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    retriveSongs();

            } else {
                Toast.makeText(this, getApplicationContext().getString(R.string.Grabando), Toast.LENGTH_SHORT).show();
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
            return;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentRelease);
        // TODO: Update with your API key
        Mint.initAndStartSession(MainActivity.this, "89af2ffd");
        setContentView(R.layout.activity_main);
        configuraciones = new Configuraciones();
        if ((isExternalStorageReadable()) && (isExternalStorageWritable())){
            File dir = new File(configuraciones.getRuta());
            if ((!dir.exists()) || (!dir.isDirectory())){
                dir.mkdirs();
            }
            validarRutaGrabacion();

            TareaAsincrona tareaAsincrona = new TareaAsincrona("RetreiveSong");
            tareaAsincrona.execute();

        }
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RecordService.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);

        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(this.getString(R.string.interstial_grabadorDeVoz));

        AdRequest adRequest2 = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        interstitial.loadAd(adRequest2);
        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                super.onAdLoaded();
            }

            @Override
            public void onAdClosed() {
                /*cuando cierren el anuncio, llamamos o mostramos el fragment de configuraciones*/
                super.onAdClosed();
            }

        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
        if (isMyServiceRuning(RecordService.class)){
            ListenerUnbindService();
        }
        deleteAllFile(configuraciones.getRecordingRoute(this), ".raw");
        Log.e("onDestroy", "destroy");
    }
    @Override
    public void onBackPressed() {
        /*disminuimos en 1 el screenNumber*/
        screenNumber = screenNumber - 1;
        getPrincipalFragment();
        try {
            if  (( (screenNumber <= 0) && (isMyServiceRuning(RecordService.class)))  || (principalFragment.isPlayerRuning()) ) {
                   /*si el screenNumber es igual a 0 significa que nos encontramos en la pantalla principal
                   por ende mantenemos la actividad ejecutandose en segundo plano mientras sigue grabando,
                   o mientras se esta reproduciendo algun audio, esto se coloca en try catch
                   para evitar la excepcion de null pointer con el player del reproductorFragment*/
                   this.moveTaskToBack(true);
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if  ( (screenNumber <= 0) && (isMyServiceRuning(RecordService.class)) ) {
                   /*si el screenNumber es igual a 0 significa que nos encontramos en la pantalla principal
                   por ende mantenemos la actividad ejecutandose en segundo plano mientras sigue grabando,*/
                this.moveTaskToBack(true);
            } else {
                super.onBackPressed();
            }
        }
    }
    //</editor-fold>
    @TargetApi(Build.VERSION_CODES.M)
    public void askPermissions(){
        /*si nos encontramos con una version de android igual o mayor a la 6.0 verificamos si
            tenemos los permisos para leer la memoria externa*/
        permissions.clear();
        write_permission = MainActivity.this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        read_permission = MainActivity.this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        record_permission = MainActivity.this.checkSelfPermission(Manifest.permission.RECORD_AUDIO);
        if (read_permission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (write_permission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (record_permission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissions.isEmpty() == false) {
            String[] params = permissions.toArray(new String[permissions.size()]);
            requestPermissions(params, 1);
        }
    }

    //<editor-fold desc="Otros Metodos">
    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            sdWritable = true;
            return true;
        }
        sdWritable = false;
        return false;
    }
    private void validarRutaGrabacion(){
        if (configuraciones.getRecordingRoute(MainActivity.this).equals("")){
            Log.e("vALIDAR rUTAgRABACION","NO SE HA CONFIGURADO NINGUNA RUTA");
            configuraciones.saveRecordingRoute(MainActivity.this,configuraciones.getRuta());
        } else {
            Log.e("vALIDAR rUTAgRABACION",configuraciones.getRecordingRoute(MainActivity.this));
        }
    }
    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
    public Audio getAudioPosicion(String audio){
        return arrayList.get(audios.indexOf(audio));
    }
    private void getReproductorFragment (){
        FragmentManager manager = getSupportFragmentManager();
        reproductorFragment = (ReproductorFragment) manager.findFragmentByTag("Reproductor");
    }
    private void getPrincipalFragment (){
        FragmentManager manager = getSupportFragmentManager();
        principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");
    }

    public void retriveSongs(){
        /*inicializamos el arrayList que va a almacenar todos los archivos grabados*/
        arrayList = new ArrayList<Audio>();
        audios = new ArrayList<>();
        /*luego configuramos para solamente obtener archivos de audio*/
        // String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        /*configuracion de las variables que van a ser pasadas al cursor para realizar las consultas*/
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND " +
                MediaStore.Audio.Media.DATA + " LIKE '"+configuraciones.getRuta()+"%'";
        /*especificamos la informacion que deseamos obtener de cada archivo de audio*/
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATE_MODIFIED

        };
        /*creamos un cursor para recorrer todos los archivos de audio de la memoria sd*/
        Cursor cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);


        /*recorremos el archivo y vamos agregandolos al listview*/
        while (cursor.moveToNext()) {
            /*creamos un arrayList para almacenar todos los nombres de las canciones
            * esto con el fin de llevar un conteo de los audios para el nombramiento
            * del nuevo audio*/
            //stringArrayList.add(cursor.getString(2));
            /*escaneo para actualizar el media store y asi visualizar la nueva grabacion realizada*/
            MediaScannerConnection.scanFile(this,
                    new String[] { cursor.getString(3) }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri.getPath());

                        }
                    });
            /*obtener la fecha de inclusion del audio*/
            File file = new File(cursor.getString(3));
            String fileDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(
                    new Date(file.lastModified())
            ).toString();
            /*calculo del peso del audio*/
            String notation = "";
            double KBsize= cursor.getDouble(1) / 1024;
            double original_Size = cursor.getDouble(1);
            /*si el calculo del peso es equivalente a 1mb o mas, cambiamos la notacion
            y almacenamos en el string solo 2 decimales, de igual manera
            se verifica si el audio es equivalente a 1gb o mas, para realizar el mismo
            procedimiento
             */
            if (KBsize > 999){
                KBsize = KBsize / 1024;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMinimumFractionDigits(2);
                formatter.setMaximumFractionDigits(2);
                notation = formatter.format(KBsize)+" MB";

                if (KBsize > 999){
                    KBsize = KBsize / 1024;
                    formatter = NumberFormat.getNumberInstance();
                    formatter.setMinimumFractionDigits(2);
                    formatter.setMaximumFractionDigits(2);
                    notation = formatter.format(KBsize)+" GB";

                }
            }
            else {
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMinimumFractionDigits(2);
                formatter.setMaximumFractionDigits(2);
                notation = formatter.format(KBsize)+" KB";
            }
            /*calculo de la duracion del audio, media store devuelve un
            * long en milisegundos, se transforma a horas:minutos:segundos
            * y se crea un string concatenando todoo*/
            long seconds= TimeUnit.MILLISECONDS.toSeconds(cursor.getLong(5));
            long minutes= TimeUnit.MILLISECONDS.toMinutes(cursor.getLong(5));
            long hours= TimeUnit.MILLISECONDS.toHours(cursor.getLong(5));
            /*procedemos luego a evaluar la cantidad de minutos y segundos, para adaptarlos
            al formato conocido por todos 60 = 1;
             */
            if (seconds >= 60){
                /*obtenemos el modulo el cual dara los segundos despues de pasar
                60
                 */
                seconds = seconds % 60;
            }
            if (minutes >= 60){
                minutes = minutes % 60;
            }
            StringBuilder duration = new StringBuilder();
            duration.append(hours);
            duration.append(minutes);
            duration.append(":");
            duration.append(seconds);



            /*agregamos al array list un objeto de tipo Audio Tape por cada cancion, pasando como parametro el nombre de la cancion,
            el artista, la duracion la fecha de creacion, etc.
             */

            arrayList.add(0,new Audio(
                    cursor.getString(0),/*ID*/
                    notation, /*SIZE*/
                    cursor.getString(2),/*TITLE*/
                    cursor.getString(3), /*DATA*/
                    cursor.getString(4),/*DISPLAY_NAME*/
                    duration.toString(),/*DURATION*/
                    fileDate,/*DATE_MODIFIED*/
                    original_Size));/*ORIGINAL SIZE*/

            /*guardamos uno a uno los nombres de todos los audios en un array list el cual
            permitira retirar la posicion de una cancion*/
            audios.add(0,cursor.getString(4));
        }

    }
    public void cargarAudioFile(String newAudioTape) {
         /*este metodo sirve para crear un objeto AudioTape, y mandar luego de finalizar la grabacion
        al usuario al fragment para reproducir el nuevo audio grabado
         */
         /*configuracion de las variables que van a ser pasadas al cursor para realizar las consultas
         * y buscamos unicamente el nuevo archivo almacenado en la Media Store*/
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND " +
                MediaStore.Audio.Media.DATA + " LIKE '" + newAudioTape + "%'";
        /*especificamos la informacion que deseamos obtener de cada archivo de audio*/
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATE_MODIFIED

        };
        /*creamos un cursor para recorrer todos los archivos de audio de la memoria sd interna*/
        Cursor cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        /*recorremos el archivo y vamos agregandolos al listview*/
        while (cursor.moveToNext()) {
            /*obtener la fecha de inclusion del audio*/
            File file = new File(cursor.getString(3));
            String fileDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(
                    new Date(file.lastModified())
            ).toString();
            /*calculo del peso del audio*/
            String notation = "";
            double KBsize = cursor.getDouble(1) / 1024;
            double original_Size = cursor.getDouble(1);
            /*si el calculo del peso es equivalente a 1mb o mas, cambiamos la notacion
            y almacenamos en el string solo 2 decimales, de igual manera
            se verifica si el audio es equivalente a 1gb o mas, para realizar el mismo
            procedimiento
             */
            if (KBsize > 999) {
                KBsize = KBsize / 1024;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMinimumFractionDigits(2);
                formatter.setMaximumFractionDigits(2);
                notation = formatter.format(KBsize) + " MB";

                if (KBsize > 999) {
                    KBsize = KBsize / 1024;
                    formatter = NumberFormat.getNumberInstance();
                    formatter.setMinimumFractionDigits(2);
                    formatter.setMaximumFractionDigits(2);
                    notation = formatter.format(KBsize) + " GB";

                }
            } else {
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMinimumFractionDigits(2);
                formatter.setMaximumFractionDigits(2);
                notation = formatter.format(KBsize) + " KB";
            }
            /*calculo de la duracion del audio, media store devuelve un
            * long en milisegundos, se transforma a horas:minutos:segundos
            * y se crea un string concatenando todoo*/
            long seconds = TimeUnit.MILLISECONDS.toSeconds(cursor.getLong(5));
            long minutes = TimeUnit.MILLISECONDS.toMinutes(cursor.getLong(5));
            long hours = TimeUnit.MILLISECONDS.toHours(cursor.getLong(5));
            /*procedemos luego a evaluar la cantidad de minutos y segundos, para adaptarlos
            al formato conocido por todos 60 = 1;
             */
            if (seconds >= 60) {
                /*obtenemos el modulo el cual dara los segundos despues de pasar
                60
                 */
                seconds = seconds % 60;
            }
            if (minutes >= 60) {
                minutes = minutes % 60;
            }
            StringBuilder duration = new StringBuilder();
            duration.append(hours);
            duration.append(minutes);
            duration.append(":");
            duration.append(seconds);



            /*agregamos al array list un objeto de tipo Audio Tape por cada cancion, pasando como parametro el nombre de la cancion,
            el artista, la duracion la fecha de creacion, etc.
             */
            audioTape = new Audio(
                    cursor.getString(0),/*ID*/
                    notation, /*SIZE*/
                    cursor.getString(2),/*TITLE*/
                    cursor.getString(3), /*DATA*/
                    cursor.getString(4),/*DISPLAY_NAME*/
                    duration.toString(),/*DURATION*/
                    fileDate,/*DATE_MODIFIED*/
                    original_Size);/*ORIGINAL SIZE*/
            Log.e("PRUEBA AUDIO", audioTape.getNAME());

        }
    }
    public void deleteAllFile(String folder, String ext) {
        Log.e("delete files","deletee: "+ext);
        GenericExtFilter filter = new GenericExtFilter(ext);
        File dir = new File(folder);

        //list out all the file name with .txt extension
        String[] list = dir.list(filter);

        if (list == null) return;

        if (list.length == 0) return;

        File fileDelete;

        for (String file : list){
            String temp = new StringBuffer(folder)
                    .append(File.separator)
                    .append(file).toString();
            fileDelete = new File(temp);
            boolean isdeleted = fileDelete.delete();
            System.out.println("file : " + temp + " is deleted : " + isdeleted);
        }
    }

    //</editor-fold>
    //<editor-fold desc="Metodos de Interfaz "Comunicacion"">
    @Override
    public void cambiarPantalla(String opcion) {
        switch (opcion){
            case "Ajustes":{
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                AjustesFragment ajustesFragment = AjustesFragment.newInstance();
                fragmentTransaction
                        .replace(R.id.ActivityFragmentHolder, ajustesFragment, "Ajustes" )
                        .commit();
                break;
            }
            case "Principal":{
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                PrincipalFragment principalFragment = PrincipalFragment.newInstance(arrayList);
                fragmentTransaction
                        .replace(R.id.ActivityFragmentHolder, principalFragment, "Principal" )
                        .commit();
                screenNumber = screenNumber + 1;
                break;
            }
            case "Reproductor":{
                /*FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                ReproductorFragment reproductorFragment = ReproductorFragment.newInstance(getAudioPosicion(getAudioSeleccionado()));
                fragmentTransaction
                        .replace(R.id.ActivityFragmentHolder, reproductorFragment, "Reproductor" )
                        .addToBackStack("Principal")
                        .commit();
                screenNumber = screenNumber + 1;*/
                /*guardo en una variable local llamada audio seleccionado el audio que selecciono el usuario en el recycler view,
                este audio lo tengo de la forma antigua como lo obtenia antes de pasarselo al fragment reproductor*/
                try {
                    this.audioSeleccionado = getAudioPosicion(getAudioSeleccionado());
                    getPrincipalFragment();
                /*llamo al metodo de principalFragment para actualizar el audio del reproductor*/
                    principalFragment.actualizarAudioReproductor(getAudioPosicion(getAudioSeleccionado()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case "Informacion": {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                InformacionFragment informacionFragment = InformacionFragment.newInstance();
                fragmentTransaction
                        .replace(R.id.ActivityFragmentHolder, informacionFragment, "Informacion" )
                        .addToBackStack("Principal")
                        .commit();
                screenNumber = screenNumber + 1;
                break;
            }

        }
    }
    @Override
    public void openFilePicker(int opcion, String opc){
        // This always works
        this.opcion = opc;
        Intent i = new Intent(this.getApplicationContext(), FilePickerActivity.class);
        // This works if you defined the intent filter
        // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

        // Set these depending on your use case. These are the defaults.
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, true);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_DIR);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, configuraciones.getRuta());

        startActivityForResult(i, opcion);
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*codigo del api FilePickerActivity para trabajar con la informacion extraida cuando se selecciona un archivo o carpeta*/
        if (resultCode == Activity.RESULT_OK) {
            Log.e("ON ACTIVITY RESULT",requestCode+"");
            /*opcion para la recuperacion de la direccion de la carpeta seleccionada por el usuario*/
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                /*se evaluan las versiones del sdk y se instancia un objeto de la clase settings para
                 salvar la direccion de almacenamiento de las grabaciones */
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();

                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            Uri uri = clip.getItemAt(i).getUri();
                            /*guardamos la ruta de la carpeta seleccionada en la variable string directory
                            y posteriormente dependiendo el codigo pasado en requestCode, ejecutamos la clase Async, pasando por parametro
                            los distintos parametros*/
                            if ((requestCode == 1) || (requestCode == 2)){
                                /*si entramos aqui, significa que este metodo fue invocado por el fragment reproductor
                                para copiar o mover algun audio*/
                                getReproductorFragment();
                                if (reproductorFragment != null){
                                    reproductorFragment.setRutaParaNuevoArchivo(uri.getPath().toString());
                                    reproductorFragment.ejecutarAccion(opcion);
                                }
                            } else if(requestCode == 3){
                                Log.e("RUTA PARA Archivos",uri.getPath().toString());
                                configuraciones.saveRecordingRoute(MainActivity.this,uri.getPath().toString());
                            }


                        }
                    }
                    // For Ice Cream Sandwich
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);

                    if (paths != null) {
                        for (String path: paths) {
                            Uri uri = Uri.parse(path);
                            /*guardamos la ruta de la carpeta seleccionada en la variable string directory
                            y posteriormente dependiendo el codigo pasado en requestCode, ejecutamos la clase Async, pasando por parametro
                            los distintos parametros
                             */
                            if ((requestCode == 1) || (requestCode == 2)){
                                getReproductorFragment();
                                if (reproductorFragment != null){
                                    reproductorFragment.setRutaParaNuevoArchivo(uri.getPath().toString());
                                    reproductorFragment.ejecutarAccion(opcion);
                                }
                            }
                            else if(requestCode == 3){
                                Log.e("RUTA PARA Archivos",uri.getPath().toString());
                                configuraciones.saveRecordingRoute(MainActivity.this,uri.getPath().toString());
                            }
                        }
                    }
                }

            } else {
                Uri uri = data.getData();
                /*guardamos la ruta de la carpeta seleccionada en la variable string directory
                            y posteriormente dependiendo el codigo pasado en requestCode, ejecutamos la clase Async, pasando por parametro
                            los distintos parametros   */
                if ((requestCode == 1) || (requestCode == 2)){
                    getReproductorFragment();
                    if (reproductorFragment != null){
                        reproductorFragment.setRutaParaNuevoArchivo(uri.getPath().toString());
                        reproductorFragment.ejecutarAccion(opcion);
                    }
                } else if(requestCode == 3){
                    Log.e("RUTA PARA Archivos",uri.getPath().toString());
                    configuraciones.saveRecordingRoute(MainActivity.this,uri.getPath().toString());
                }
            }
        }

    }
    @Override
    public String getAudioSeleccionado() {
        return audio;
    }
    @Override
    public void setAudioSeleccionado(String audio) {
        this.audio = audio;
    }
    @Override
    public void changeAudioArrayList(Audio audioOriginal, Audio audioNuevo) {
        /*cambiamos los datos de la lista de audios y la lista que guarda los nombres de cada audio*/
        try {
            for (int i = 0; i< arrayList.size();i++){
                if (arrayList.get(i).getDATA().equals(audioOriginal.getDATA())){
                    arrayList.set(i, audioNuevo);
                    audios.set(i,audioNuevo.getNAME());
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void removeAudioArrayList(Audio audioOriginal) {
        /*metodo utilizado para eliminar de la lista audios el nombre del archivo que ya fue eliminado
        * en los metodos del recycler view, la lista arraylist no se toca pq la misma es afectada
        * al ser pasada por parametro al resto de los fragments, es decir ese parametro no representa una copia
        * representa el propio objeto, por ende si se modifica alla tambien se ve reflejado aqui*/
        for (int i = 0; i< audios.size();i++){
            if (audios.get(i).equals(audioOriginal.getNAME())){
                audios.remove(i);
                return;
            }
        }
    }

    @Override
    public void agregarAudio(Audio nuevoAudio) {
        /*agregamos nuevo audio en las 2 listas encargadas de gestionar el numero de grabaciones en la aplicacion*/
        arrayList.add(0,nuevoAudio);
        audios.add(0,nuevoAudio.getNAME());

    }

    @Override
    public boolean readableFlag() {
        return sdWritable;
    }

    @Override
    public boolean isMyServiceRuning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public RecordService getService() {
        return mService;
    }

    @Override
    public void ListenerBindService(Intent intent) {
        Log.e("ListenerBindService", "Deberia bind el servicio");
        MainActivity.this.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void ListenerUnbindService() {
        if (mBound) {
            MainActivity.this.unbindService(mConnection);
            stopService(new Intent(this, RecordService.class));
            mBound = false;
        }
    }
    @Override
    public ArrayList<Audio> getListadoAudios() {
        return this.arrayList;
    }

    @Override
    public boolean returnInterstecialOpened() {
        return advertisingLoad;
    }

    @Override
    public void mostrarInterstecial() {
        /*si el anuncio fue cargado lo mostramos*/
            if ((interstitial.isLoaded()) && (returnInterstecialOpened() == false) ) {
                interstitial.show();
                advertisingLoad = true;
            }
        /*si no fue mostrado mostramos el fragment de configuraciones*/

    }

    @Override
    public Audio getAudioSeleccionadoParaFragmentReproductor() {
        return this.audioSeleccionado;
    }

    @Override
    public ArrayList<String> getListadoDeNombresDeAudios() {
        /*retornamos la lista de nombres de audios, para actualizarla cuando se ordene el listado de archivos*/
        return this.audios;
    }

    @Override
    public void ordenarArchivos(String opcion) {
        getPrincipalFragment();
        principalFragment.ordenarArchivos(opcion);
    }

    //</editor-fold>
    //<editor-fold desc="Clases y ServiceConnection">
    private class TareaAsincrona extends AsyncTask<Void, Void, Void>{
        String opcion;

        public TareaAsincrona(String opcion) {
            this.opcion = opcion;
        }
        @Override
        protected Void doInBackground(Void... params) {
            switch(opcion){
                case "RetreiveSong":{
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        askPermissions();
                    else
                        retriveSongs();
                    break;
                }
                case "CargarAudio":{
                    cargarAudioFile(audioFile);
                    break;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            switch (opcion){
                case "CargarAudio":{
                /*obtenemos una referencia del fragment principal y posteriormente mandamos el nuevo audio
                    * registrado para actualizar el recycler view*/
                    getPrincipalFragment();
                    principalFragment.agregarAudio(audioTape);
                    /*adicionalmente, llamamos al metodo cerrarMensajeCodificacion, para quitar el mensaje
                    de codificando audio, y adicionalmente mandamos el nuevo audio para mostrar el link
                    en la pantalla grabacion por si el usuario desea escuchar la nueva grabacion*/
                    principalFragment.cerrarMensajeCodificacion(audioTape);
                    break;
                }
                case "RetreiveSong":{
                    cambiarPantalla(getString(R.string.CambiarPAntallaPrincipal));
                    break;
                }
            }
        }
    }
    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            RecordService.LocalBinder binder = (RecordService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            mService.setActivity(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int opcion = intent.getIntExtra("Opcion",0);
            switch (opcion){
                case 0:{
                    /*recibimos el broadcast por parte del servicio de grabacion, lo siguiente es desactivar
                    dicho servicio*/
                    ListenerUnbindService();
                    /*obtenemos la informacion del nuevo audio creado desde el servicio a traves del intent*/
                    audioFile = intent.getStringExtra("AudioFile");
                    /*creamos una tarea asincrona para registrar este nuevo audio en el MediaScanner*/
                    new TareaAsincrona("CargarAudio").execute();
                    break;
                }
            }
        }
    }
    public class GenericExtFilter implements FilenameFilter {
        /*clase creada para la limpieza de archivos .raw que no fueron correctamente eliminados*/
        private String ext;

        public GenericExtFilter(String ext) {
            this.ext = ext;
        }

        public boolean accept(File dir, String name) {
            return (name.endsWith(ext));
        }
    }
    //</editor-fold>
}
