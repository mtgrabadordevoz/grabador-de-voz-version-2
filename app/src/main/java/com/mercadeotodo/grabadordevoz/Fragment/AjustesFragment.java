package com.mercadeotodo.grabadordevoz.Fragment;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mercadeotodo.grabadordevoz.Clases.MyApplication;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

public class AjustesFragment extends Fragment implements View.OnClickListener {
    private Comunicacion mListener;
    private Button bAjustesCarpetas, bAjustesInformacion, bAjustesRate, bAjusteShare;
    public AjustesFragment() {
        // Required empty public constructor
    }

    public static AjustesFragment newInstance() {
        AjustesFragment fragment = new AjustesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Fragment Ajustes");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ajustes, container, false);
        inicializarComponentes(view);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (Comunicacion) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_ajustes_button_carpetas:{
                mListener.mostrarInterstecial();
                mListener.openFilePicker(3,"");

                break;
            }
            case R.id.fragment_ajustes_button_ayuda: {
                mListener.mostrarInterstecial();
                mListener.cambiarPantalla("Informacion");

                break;
            }
            case R.id.fragment_ajustes_button_rate: {
                showRateDialog();
                break;
            }
            case R.id.fragment_ajustes_button_share: {
                shareApp();
                break;
            }
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void inicializarComponentes(View view) {

        bAjustesCarpetas = (Button) view.findViewById(R.id.fragment_ajustes_button_carpetas);
        bAjustesCarpetas.setOnClickListener(this);
        bAjustesInformacion = (Button) view.findViewById(R.id.fragment_ajustes_button_ayuda);
        bAjustesInformacion.setOnClickListener(this);
        bAjustesRate = (Button) view.findViewById(R.id.fragment_ajustes_button_rate);
        bAjustesRate.setOnClickListener(this);
        bAjusteShare = (Button) view.findViewById(R.id.fragment_ajustes_button_share);
        bAjusteShare.setOnClickListener(this);
    }

    public void showRateDialog(){
        /*metodo que muestra un alert dialog para que el usuario confirme si desea
               * eliminar los audios seleccionados */
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setTitle(getActivity().getString(R.string.RateThisApp));
        alertBuilder.setMessage(getActivity().getString(R.string.RateMessage));
        /*se configura el alertDialogBuilder*/
        alertBuilder.setPositiveButton(getActivity().getString(R.string.Rate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*abrimos el link a la aplicacion*/
                goToMyApp(true);
            }
        });

        alertBuilder.setNegativeButton(getActivity().getString(R.string.NoRate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        /*se instancia ahora un AlertDialog partiendo del AlertDialogBuilder*/
        final AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }
    public void goToMyApp(boolean googlePlay) {//true if Google Play, false if Amazone Store
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse((googlePlay ? "market://details?id=" : "amzn://apps/android?p=") +getActivity().getPackageName())));
        } catch (ActivityNotFoundException e1) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse((googlePlay ? "http://play.google.com/store/apps/details?id=" : "http://www.amazon.com/gp/mas/dl/android?p=") +getActivity().getPackageName())));
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(getActivity(), "You don't have any app that can open this link", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void shareApp(){
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Grabador de Voz Plus");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id="+getActivity().getPackageName());

            getActivity().startActivity(Intent.createChooser(emailIntent, getActivity().getString(R.string.ShareApp)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.Error_Shared), Toast.LENGTH_SHORT).show();
        }
    }
}
