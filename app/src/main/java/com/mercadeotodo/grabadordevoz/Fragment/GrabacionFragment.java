package com.mercadeotodo.grabadordevoz.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mercadeotodo.grabadordevoz.Clases.Audio;
import com.mercadeotodo.grabadordevoz.Clases.Configuraciones;
import com.mercadeotodo.grabadordevoz.Clases.MyApplication;
import com.mercadeotodo.grabadordevoz.Clases.RecordService;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;
import com.triggertrap.seekarc.SeekArc;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GrabacionFragment extends Fragment implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 3;
    private Comunicacion mListener;
    private SeekArc seekBar;
    private Chronometer chronometer;
    private Button bGrabar, bDetener;
    private Handler mHandler = new Handler();
    // flag that should be set true if handler should stop
    boolean mStopHandler = true;
    private int valorSeekBar = 0;
    private long timeWhenStopped = 0;
    private Configuraciones configuraciones;
    private String nuevaGrabacion = "";
    private Intent intentMyIntentService;
    private PrincipalFragment principalFragment;
    private List<Audio> listSongs;
    private ProgressBar progressBar;
    private TextView textView, textNuevoAudio;

    /*variables para verificar los perisos para android 6.0*/
    private int read_permission = 0;
    private int write_permission = 0;
    private int record_permission = 0;
    List<String> permissions = new ArrayList<String>();

    private boolean permisos = true;

    public GrabacionFragment() {
        // Required empty public constructor
    }

    public static GrabacionFragment newInstance() {
        GrabacionFragment fragment = new GrabacionFragment();
        Bundle args = new Bundle();
       fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    grabar();

            } else {
                Toast.makeText(getActivity(), getActivity().getString(R.string.Permisos), Toast.LENGTH_SHORT).show();
                permisos = false;
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
            return;
        }

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        validarRegreso();
        MyApplication.getInstance().trackScreenView("Fragment Grabacion");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grabacion, container, false);
        inicializarComponentes(view);
        return view;
    }



    private void inicializarComponentes(View view) {
        seekBar = (SeekArc) view.findViewById(R.id.fragmentGrabacionseekArc);
        seekBar.setMax(1000);
          /*desactivamos el touch del seekbar, este metodo fue agregado por mi*/
        seekBar.setFlagTouch(false);
        chronometer = (Chronometer) view.findViewById(R.id.chronometer);
        bGrabar = (Button) view.findViewById(R.id.buttonRecord);
        /*le asignamos un tag para verificar si debemos pausar o seguir grabando*/
        bGrabar.setTag(R.drawable.ic_fiber_manual_record_black_48dp);
        bDetener = (Button) view.findViewById(R.id.buttonStop);
        bDetener.setEnabled(false);
        bGrabar.setOnClickListener(this);
        bDetener.setOnClickListener(this);
        configuraciones = new Configuraciones();
        progressBar = (ProgressBar) view.findViewById(R.id.fragmentGrabacionProgresBar);
        progressBar.setVisibility(View.GONE);
        textView = (TextView) view.findViewById(R.id.fragmentGrabacionTextViewCodificando);
        textView.setText(getActivity().getString(R.string.CodificandoAudio));
        textView.setVisibility(View.GONE);
        textNuevoAudio = (TextView) view.findViewById(R.id.fragmentGrabacionTextViewNuevoAudio);
        textNuevoAudio.setVisibility(View.GONE);
        textNuevoAudio.setOnClickListener(this);

    }
    public void desactivarMensajeCodificacion(Audio nuevoAudio){
        if (chronometer != null) {
            chronometer.setVisibility(View.VISIBLE);
        }
        if (bDetener != null) {
            bDetener.setVisibility(View.VISIBLE);
        }
        if (bGrabar != null) {
            bGrabar.setVisibility(View.VISIBLE);
        }
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        if (textView != null) {
            textView.setVisibility(View.GONE);
        }
        if (nuevoAudio != null) {
            textNuevoAudio.setText(nuevoAudio.getNAME());
            textNuevoAudio.setVisibility(View.VISIBLE);
        }
    }
    public void activarMensajeCodificacion(){
        chronometer.setVisibility(View.GONE);
        bDetener.setVisibility(View.GONE);
        bGrabar.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

            mListener = (Comunicacion) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonRecord:{
                switch(bGrabar.getTag().hashCode()){
                    /*evaluamos el resource asignado al imgButtonGrabar, de acuerdo a la informacion
                    que se encuentre en el tag del imgButton, y dependiendo de la imagen que tenga asignada
                    realizamos un metodo en especifico
                     */
                    case R.drawable.ic_fiber_manual_record_black_48dp:{
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            /*si nos encontramos en un android 6.0 o superior, verificamos que tengamos
                            todos los permisos correspondientes para grabar */
                            read_permission = getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                            write_permission = getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                            record_permission = getActivity().checkSelfPermission(Manifest.permission.RECORD_AUDIO);
                            if (read_permission != PackageManager.PERMISSION_GRANTED) {
                                permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                            }
                            if (write_permission != PackageManager.PERMISSION_GRANTED) {
                                permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                            }
                            if (record_permission != PackageManager.PERMISSION_GRANTED) {
                                permissions.add(Manifest.permission.RECORD_AUDIO);
                            }
                            if (!permissions.isEmpty()) {
                                String[] params = permissions.toArray(new String[permissions.size()]);
                                requestPermissions(params, 1);
                            }

                        } else {
                            grabar();
                        }

                        break;
                    }
                    case R.drawable.ic_action_pause:{
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        } else {
                            pausar();
                        }
                        break;
                    }
                }
                break;
            }
            case R.id.buttonStop:{
                detener();
                break;
            }
            case R.id.fragmentGrabacionTextViewNuevoAudio: {
                mListener.setAudioSeleccionado(textNuevoAudio.getText().toString());
                mListener.cambiarPantalla(getActivity().getString(R.string.CambiarPAntallaReproductor));
                break;
            }
        }
    }

    private void getPrincipalFragment(){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");
        listSongs = principalFragment.getAudios();

    }
    private boolean getRecorderName(String record){
        for (int i = 0;i< mListener.getListadoAudios().size(); i++){
            if (mListener.getListadoAudios().get(i).getDATA().equals(record)){
                return true;
            }
        }
        return false;
    }
    public void validarRegreso(){
        /*verificamos si el servicio de grabacion esta corriendo para la correcta
        inicializacion de los botones*/
        textNuevoAudio.setVisibility(View.GONE);
        if (mListener.isMyServiceRuning(RecordService.class)) {
            bDetener.setEnabled(false);
            try {

                if (mListener.getService().isEncodingFile()) {
                    activarMensajeCodificacion();
                } else if (mListener.getService().isPauseBottonPresse()) {
                    bGrabar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fiber_manual_record_black_48dp, 0, 0, 0);
                    bGrabar.setTag(R.drawable.ic_fiber_manual_record_black_48dp);
                    bDetener.setEnabled(true);
                    /*mandamos nulo pq en este instante el audio que se esta grabando no esta listo
                    * para colocar el link al final del fragment reproductor*/
                    desactivarMensajeCodificacion(null);
                } else {
                    bGrabar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_pause,0,0,0);
                    bGrabar.setTag(R.drawable.ic_action_pause);
                    bDetener.setEnabled(true);
                    Date now = new Date();
                    long elapsedTime = now.getTime() - configuraciones.getStartedTime(getActivity()); //startTime is whatever time you want to start the chronometer from. you might have stored it somwehere
                    chronometer.setBase(SystemClock.elapsedRealtime() - elapsedTime);
                    chronometer.start();
                    mStopHandler = false;
                    // iniciamoz el handler para actualizar la interfaz grafica
                    mHandler.post(runnable);
                    /*mandamos nulo pq en este instante el audio que se esta grabando no esta listo
                    * para colocar el link al final del fragment reproductor*/
                    desactivarMensajeCodificacion(null);
                }
            } catch (Exception e) {
                bGrabar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_pause,0,0,0);
                bGrabar.setTag(R.drawable.ic_action_pause);
                bDetener.setEnabled(true);
                e.printStackTrace();
            }
        } else {
            bDetener.setEnabled(false);
            bGrabar.setEnabled(true);
        }
    }

    //<editor-fold desc="Metodos de Grabacion">
    public void grabar(){

        textNuevoAudio.setVisibility(View.GONE);
        bGrabar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_pause, 0, 0, 0);
        bGrabar.setTag(R.drawable.ic_action_pause);
        /*mandamos nulo pq en este instante el audio que se esta grabando no esta listo
         * para colocar el link al final del fragment reproductor*/
        desactivarMensajeCodificacion(null);

        if (mListener.isMyServiceRuning(RecordService.class)){
            /*si el servicio aun esta corriendo cuando se presione play
            significa que se dio pause a la grabacion, por ende se llama luego
            al metodo del servicio reasumeRecording para continuar con la grabacion   */
            mListener.getService().reasumeRecording();
            /*se reinicia el cronometro tomando los datos de cuando fue detenido*/
            chronometer.setBase(SystemClock.elapsedRealtime() + configuraciones.getPauseTime(getActivity()));
            chronometer.start();
            mStopHandler = false;
            // iniciamoz el handler para actualizar la interfaz grafica
            mHandler.post(runnable);
            bDetener.setVisibility(View.VISIBLE);
            bDetener.setEnabled(true);
        } else {
            /*iniciamos el cronometro, ponemos la variable booleana stopHandler en falso y mandamos a correr el handler
            para actualizar la inferfaz grafica*/
            if (mListener.readableFlag()){
                File dir;
                dir = new File(configuraciones.getRecordingRoute(getActivity()));
                if (dir.exists() && dir.isDirectory()){
                    /*obtenemos una referencia al fragment principal y obtenemos el listado de todos los audios
                    registrados en el sistema*/
                    getPrincipalFragment();
                    /*creamos una variable entera que contenga el numero de archivos registrados*/
                    int totalAudioList = listSongs.size();
                    /*agregamos el valor de la variable entera a la ruta del nuevo audio a registrar*/
                    nuevaGrabacion = dir.getAbsolutePath()+"/record-"+totalAudioList;
                    /*vamos ajustando la ruta del nuevo archivo hasta que sea unica, para evitar
                    sobre escribir alguna grabacion*/
                    while (getRecorderName(nuevaGrabacion)) {
                            totalAudioList = totalAudioList + 1;
                            nuevaGrabacion = dir.getAbsolutePath()+"/record-"+totalAudioList;
                    }

                    startBackgroundService();
                }
            }
            chronometer.setBase(SystemClock.elapsedRealtime());
            Date now = new Date();
            configuraciones.saveStartedTime(getActivity(),now.getTime());
            timeWhenStopped = 0;
            chronometer.start();
            mStopHandler = false;
            // iniciamoz el handler para actualizar la interfaz grafica
            mHandler.post(runnable);
            bDetener.setEnabled(true);
        }
    }
    public void startBackgroundService(){
        /*metodo para verificar iniciar el service de grabacion*/
                /*
                * Creates a new Intent to start the RSSPullService
                * IntentService. Passes a URI in the
                * Intent's "data" field. */
        /*creacion del intent que mandaremos al servicio, junto con la informacion del audio a grabar*/
        intentMyIntentService = new Intent(getActivity(), RecordService.class);
        intentMyIntentService.putExtra(RecordService.EXTRA_KEY_IN, nuevaGrabacion);
        /*inicio o activacion del servicio*/
        getActivity().startService(intentMyIntentService);
        mListener.ListenerBindService(intentMyIntentService);

    }
    public void detener(){
        try {
            mListener.getService().stopRecording();
        } catch (Exception e) {
            getActivity().stopService(new Intent(getActivity(), RecordService.class));
            e.printStackTrace();
        }
        /*detenemos y reiniciamos el cronometro para que muestre 0 al usuario*/
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        /*colocamos la variable booleana para detener el handler en true y reiniciamos los valores del seekbar
        y la variable utilizada para darle progreso*/
        mStopHandler = true;
        mHandler.removeCallbacks(runnable);
        valorSeekBar = 0;
        seekBar.setProgress(0);
        bDetener.setEnabled(false);
        /*cambiamos la imagen del boton, el orden left, top,right,botom, colocando 0 donde no queremos la imagen*/
        bGrabar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fiber_manual_record_black_48dp,0, 0, 0);
        bGrabar.setTag(R.drawable.ic_fiber_manual_record_black_48dp);

        activarMensajeCodificacion();
    }
    public void pausar(){
        if (mListener.getService() != null) {
            bDetener.setEnabled(true);
            mListener.getService().pauseRecording();
       /*detenemos el cronometro y guardamos el tiempo en que se detuvo*/
            chronometer.stop();
            timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            configuraciones.savePauseTime(getActivity(),timeWhenStopped);
        /*cambiamos la imagen del boton colocando la imagen donde queremos (left) y 0 para el resto (top,botom,right)*/
            bGrabar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fiber_manual_record_black_48dp,0,  0, 0);
            bGrabar.setTag(R.drawable.ic_fiber_manual_record_black_48dp);
        /*cambiamos la bandera del seekbar para que se detenga tambien, pero sin reiniciar sus valores*/
            mStopHandler = true;
            mHandler.removeCallbacks(runnable);
        }
    }
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // do your stuff - don't create a new runnable here!
            if (!mStopHandler) {
                /*1000 representan 1 segundo*/
                mHandler.postDelayed(this, 60);
                valorSeekBar = valorSeekBar + 1;
                seekBar.setProgress(valorSeekBar);
                if (valorSeekBar > 1000){
                    seekBar.setProgress(0);
                    valorSeekBar = 0;
                }
            }
        }
    };
    //</editor-fold>

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
