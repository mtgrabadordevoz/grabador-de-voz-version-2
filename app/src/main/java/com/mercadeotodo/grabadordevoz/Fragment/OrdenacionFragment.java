package com.mercadeotodo.grabadordevoz.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrdenacionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrdenacionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrdenacionFragment extends Fragment implements View.OnClickListener {
    private Comunicacion mListener;
    private RadioGroup radioGroup;
    private RadioButton radio_Nombre, radio_Tamano, radio_Fecha;
    private PrincipalFragment principalFragment;

    public OrdenacionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment OrdenacionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrdenacionFragment newInstance() {
        OrdenacionFragment fragment = new OrdenacionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ordenacion, container, false);
        inicializarComponentes(view);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            mListener = (Comunicacion) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void inicializarComponentes (View view){
        radioGroup = (RadioGroup) view.findViewById(R.id.fragmentOrdenacionRadioGroup);
        radio_Nombre = (RadioButton) view.findViewById(R.id.radionombre);
        radio_Tamano = (RadioButton) view.findViewById(R.id.radiotamano);
        radio_Fecha = (RadioButton) view.findViewById(R.id.radiofecha);
        radio_Fecha.setOnClickListener(this);
        radio_Tamano.setOnClickListener(this);
        radio_Nombre.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if ((v.getId() == R.id.radionombre) || (v.getId() == R.id.radiotamano)|| (v.getId() == R.id.radiofecha)  ){
            /*cuando seleccionen un radio button llamamos al metodo onRadiocheck para verificar
            que radio button tocaron*/
            onRadioButtonClicked(v);
        }
    }
    public void onRadioButtonClicked(View view) {
        /*metodo llamado desde cada radio button en el xml, desde alla mismo se establece esta propiedad*/
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked

        switch(view.getId()) {
            case R.id.radionombre: {
                if (checked) {
                    mListener.ordenarArchivos("Name");
                }
                break;
            }
            case R.id.radiotamano: {
                if (checked) {
                    mListener.ordenarArchivos("Size");
                }
                break;
            }
            case R.id.radiofecha: {
                if (checked) {
                    mListener.ordenarArchivos("Date");
                }
                break;
            }

        }
        getPrincipalFragment();
        if (principalFragment != null) {
            principalFragment.ocultarFragmentOrdenamiento();
        }
    }

    private void getPrincipalFragment (){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
