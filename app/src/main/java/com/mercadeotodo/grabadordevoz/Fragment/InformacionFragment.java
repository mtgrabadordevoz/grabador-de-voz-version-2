package com.mercadeotodo.grabadordevoz.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mercadeotodo.grabadordevoz.Clases.MyApplication;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InformacionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InformacionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InformacionFragment extends Fragment implements View.OnClickListener{

    private Comunicacion mListener;
    private TextView textAplicacionTitulo, textAplicacionDetalle, textAdministrarTitulo, textAdministrarDetalle, textGrabacionTitulo,textGrabacionDetalle;
    private Toolbar toolbar;

    public InformacionFragment() {
        // Required empty public constructor
    }

    public static InformacionFragment newInstance() {
        InformacionFragment fragment = new InformacionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().trackScreenView("Fragment Informacion");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_informacion, container, false);
        inicializarComponentes(view);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            mListener = (Comunicacion) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textInformationAdministrarArchivos : {
                textAdministrarDetalle.setVisibility( textAdministrarDetalle.isShown() ? View.GONE : View.VISIBLE );
                break;
            }
            case R.id.textInformationComoGrabar: {
                textGrabacionDetalle.setVisibility( textGrabacionDetalle.isShown() ? View.GONE : View.VISIBLE );
                break;
            }
            case R.id.textInformationLaAplicacion : {
                textAplicacionDetalle.setVisibility( textAplicacionDetalle.isShown() ? View.GONE : View.VISIBLE );
                break;
            }
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void inicializarComponentes(View view) {
        toolbar = (Toolbar) view.findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        toolbar.setTitle(getActivity().getString(R.string.FragmentAjustes_Informacion));
        AppCompatActivity activity = (AppCompatActivity) this.getActivity();
        activity.setSupportActionBar(toolbar);

        textAdministrarDetalle = (TextView) view.findViewById(R.id.textInformationAdministrarArchivosDetalle);
        textAdministrarTitulo = (TextView) view.findViewById(R.id.textInformationAdministrarArchivos);
        textAplicacionTitulo = (TextView) view.findViewById(R.id.textInformationLaAplicacion);
        textAplicacionDetalle = (TextView) view.findViewById(R.id.textInformationLaAplicacionDetalle);
        textGrabacionDetalle = (TextView) view.findViewById(R.id.textInformationComoGrabarDetalle);
        textGrabacionTitulo = (TextView) view.findViewById(R.id.textInformationComoGrabar);

        textGrabacionTitulo.setOnClickListener(this);
        textAdministrarTitulo.setOnClickListener(this);
        textAplicacionTitulo.setOnClickListener(this);

        textGrabacionDetalle.setVisibility(View.GONE);
        textAdministrarDetalle.setVisibility(View.GONE);
        textAplicacionDetalle.setVisibility(View.GONE);
    }
}
