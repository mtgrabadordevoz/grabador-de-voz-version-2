package com.mercadeotodo.grabadordevoz.Fragment;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mercadeotodo.grabadordevoz.Clases.Audio;
import com.mercadeotodo.grabadordevoz.Clases.ViewPager_PrincipalAdapter;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

import java.util.ArrayList;
import java.util.List;

public class PrincipalFragment extends Fragment {
   private Comunicacion mListener;
    private ViewPager viewPager;
    private ViewPager_PrincipalAdapter adapter;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private static ArrayList<Audio> audio;
    private MenuItem searchItem;
    private SearchView searchView;
    private MenuItem deleteItem;
    private ListadoAudioFragment listadoAudioFragment;
    public static List<MediaPlayer> listaDePlayers = new ArrayList<MediaPlayer>();
    private GrabacionFragment grabacionFragment;
    private ReproductorFragment reproductorFragment;


    public PrincipalFragment() {
        // Required empty public constructor
    }

    public static PrincipalFragment newInstance(ArrayList<Audio> listado) {
        PrincipalFragment fragment = new PrincipalFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        audio = listado;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_principal, container, false);
        inicializarComponentes(view);
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            mListener = (Comunicacion) context;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main_menu, menu);
        /*obtenemos referencia al menu para buscar y al menu para borrar*/
        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        /*colocamos invisible el sarch item para que no aparesca en el primer page*/
        searchItem.setVisible(false);

        deleteItem = menu.findItem(R.id.action_delete);
        deleteItem.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_delete:{
                /*obtenemos una referencia del fragment listadoAudio y mandamos a borrar los audios seleccionados*/
                getListadoFragment();
                listadoAudioFragment.getAdapter().iniciarVaciadoDeAudios();
                System.gc();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void ocultarFragmentOrdenamiento(){
        /*obtenemos una referencia al fragment ordenacion*/
        OrdenacionFragment ordenamientoFragment = (OrdenacionFragment) getActivity().getSupportFragmentManager().findFragmentByTag("Ordenacion");
        android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (ordenamientoFragment != null) {
            transaction.remove(ordenamientoFragment);
            transaction.commit();
        }
        getListadoFragment();
        if (listadoAudioFragment != null) {
            listadoAudioFragment.setControlFragmentOrdenacion();
        }
    }
    private void inicializarComponentes(View view) {


        final AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mAdView.setVisibility(View.GONE);
            }
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });

        viewPager = (ViewPager) view.findViewById(R.id.fragmentPrincipalPager);
        adapter = new ViewPager_PrincipalAdapter(getChildFragmentManager(), getActivity(), audio);
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) view.findViewById(R.id.fragmentPrincipalTabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_record_voice_over_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_audio_list_white);
        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_audiotrack_white_24dp);
        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_settings_white_24dp);


        /*obtener una instancia del fragment listado*/
        getListadoFragment();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                /*verificamos la pagina que se esta mostrando para mostrar o ocultar el searchitem*/
                switch (position) {
                    case 0: {
                        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_record_voice_over_black_24dp);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_audio_list_white);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_audiotrack_white_24dp);
                        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_settings_white_24dp);
                        break;
                    }
                    case 1: {
                        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_record_voice_over_white_24dp);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_audio_list_black);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_audiotrack_white_24dp);
                        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_settings_white_24dp);
                        break;
                    }
                    case 2: {
                        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_record_voice_over_white_24dp);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_audio_list_white);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_audiotrack_black_24dp);
                        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_settings_white_24dp);
                        break;
                    }
                    case 3:{
                        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_record_voice_over_white_24dp);
                        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_audio_list_white);
                        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_audiotrack_white_24dp);
                        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_settings_black_24dp);
                        break;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }
    public void ocultarIconoBorrarAudio(){
        getListadoFragment();
        listadoAudioFragment.ocultarDeleteButton();
    }
    public void mostrarIconoBorrarAudio(){
        getListadoFragment();
        listadoAudioFragment.mostrarDeleteButton();
    }

    public void editarAudio(Audio original, Audio nuevo){
        listadoAudioFragment.getAdapter().iniciarEdicionDeAudio(original, nuevo);
    }
    public void eliminarAudio(Audio audioAEliminar){
        /*mandamos a eliminar el audio recibido por parametro, este metodo es utilizado por los metodos
        de copiar y mover en el fragment reproductor, y se utiliza para eliminar el audio original
        cuando se esta moviendo a una nueva carpeta*/
        listadoAudioFragment.getAdapter().eliminarAudiosDeFormaForzada(audioAEliminar);
    }

    public void getListadoFragment(){
        listadoAudioFragment =  (ListadoAudioFragment) adapter.instantiateItem(viewPager,1);

    }
    public void getGrabacionFragment(){
        grabacionFragment = (GrabacionFragment) adapter.instantiateItem(viewPager,0);
    }
    public void getReproductorFragment(){
        reproductorFragment = (ReproductorFragment) adapter.instantiateItem(viewPager,2);
    }
    public void actualizarAudioReproductor(Audio audio){
        getReproductorFragment();
        viewPager.setCurrentItem(2);
        reproductorFragment.setNuevoAudio(audio);

    }
    public boolean isPlayerRuning(){
        /*obtenemos una referencia al fragment reproductor y verificamos a traves de su metodo
        si el player esta reproduciendo*/
        getReproductorFragment();
        return reproductorFragment.isPlayerPlaying();
    }
    public void cerrarMensajeCodificacion(Audio nuevoAudio){
        /*obtenemos una referencia al fragment hijo grabacion, mandamos a quitar el mensaje
        de codificando y adicionalmente pasamos el nuevo audio para hacer el link
        por si el usuario desea escucharlo*/
        getGrabacionFragment();
        grabacionFragment.desactivarMensajeCodificacion(nuevoAudio);
    }
    public void agregarAudio(Audio audio){
        /*recibimos el nuevo audio grabado por parametro y lo mandamos al
        adapter del recycler view para que lo registre y actualize el recycler*/
        listadoAudioFragment.getAdapter().addAudio(audio);
    }
    public List<Audio> getAudios(){
        /*retornamos todas las grabaciones registradas en el sistema*/
        return listadoAudioFragment.getAdapter().getAudios();
    }
    public List<MediaPlayer> getListaDePlayers() {
        /*devolvemos el listado de mediaPlayers para que el fragment reproductor
        verifique si hay algun audio reproduciendose*/
        return listaDePlayers;
    }
    public void ordenarArchivos (String opcion){
        getListadoFragment();
        listadoAudioFragment.getAdapter().iniciarORdenacion(opcion);
    }

}
