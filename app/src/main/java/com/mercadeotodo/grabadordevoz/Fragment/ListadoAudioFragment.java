package com.mercadeotodo.grabadordevoz.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.mercadeotodo.grabadordevoz.Clases.Audio;
import com.mercadeotodo.grabadordevoz.Clases.RecyclerView_ListadoAudioAdapter;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;

import java.util.ArrayList;
import java.util.List;

public class ListadoAudioFragment extends Fragment implements View.OnClickListener {
    private Comunicacion mListener;
    private ArrayList<Audio> audio;
    private RecyclerView_ListadoAudioAdapter adapter;
    private RecyclerView recyclerView;
    private EditText editText;
    private ImageButton imageDelete, imageOrdenar;

    private FrameLayout contenedor;
    private int controlFragmentOrdenacion = 0;
    public ListadoAudioFragment() {
        // Required empty public constructor
    }

    public static ListadoAudioFragment newInstance(ArrayList<Audio> listado) {
        ListadoAudioFragment fragment = new ListadoAudioFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        args.putParcelableArrayList("Listado", listado);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            audio = getArguments().getParcelableArrayList("Listado");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listado_audio, container, false);
        inicializarComponentes(view);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            mListener = (Comunicacion) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.fragmentListadoDelete: {
                adapter.iniciarVaciadoDeAudios();
                break;
            }
            case R.id.fragmentListadoOrdenar: {
                /*si tocan el boton ordenar ponemos visible el radio group*/

                if (controlFragmentOrdenacion % 2 == 0){
                    mostrarFragmentOrdenar();
                } else {
                    ocultarFragmentOrdenar();
                }
                controlFragmentOrdenacion ++;
                break;
            }


        }

    }

    public void setControlFragmentOrdenacion() {
        this.controlFragmentOrdenacion ++;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void inicializarComponentes(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.fragmentListadoAudioRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new RecyclerView_ListadoAudioAdapter(getActivity(),audio);
        recyclerView.setAdapter(adapter);
        imageDelete = (ImageButton) view.findViewById(R.id.fragmentListadoDelete);
        imageDelete.setOnClickListener(this);
        imageDelete.setVisibility(View.GONE);
        imageOrdenar = (ImageButton) view.findViewById(R.id.fragmentListadoOrdenar);
        imageOrdenar.setOnClickListener(this);

        contenedor = (FrameLayout) view.findViewById(R.id.fragmentListadoFrameLayout);
        editText = (EditText) view.findViewById(R.id.fragmentListadoEditText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*creamos un List el cual tendra la lista filtrada de lo que esta buscando el usuario*/
                final List<Audio> filteredModelList = adapter.BuscarAudio(s.toString());
                /*finalmente mandamos esta nueva lista filtrada para que sea la mostrada en el recyclerview*/
                adapter.setListadoAudio(filteredModelList);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        /*preparacion del edit text para buscar*/
        ImageSpan imageHint = new ImageSpan(getActivity(), R.mipmap.ic_search_black_24dp);
        SpannableString spannableString = new SpannableString(" ");
        spannableString.setSpan(imageHint, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        editText.setHint(spannableString);
    }

    public void ocultarFragmentOrdenar(){
        /*obtenemos una referencia al fragment ordenacion*/
        OrdenacionFragment ordenacionFragment = (OrdenacionFragment) getActivity().getSupportFragmentManager().findFragmentByTag("Ordenacion");
        android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (ordenacionFragment != null){
            transaction.remove(ordenacionFragment);
            transaction.commit();
        }
    }
    public void mostrarFragmentOrdenar(){
        OrdenacionFragment ordenacionFragment = OrdenacionFragment.newInstance();
        android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragmentListadoFrameLayout,ordenacionFragment,"Ordenacion");
        transaction.commit();

    }
    public List<Audio> getFiltroAudio(String filtro){
        /*metodo encargado de retornar la lista filtrada en el adapter
        * es utilizado por el fragment principal para obtener la lista filtrada de la busqueda del usuario
        * aqui retornamos el resultado devuelto por el metodo buscarAudio, y pasamos por parametro
        * el string recibido del frament principal, este string es lo ingresado en el search view*/
        return adapter.BuscarAudio(filtro);
    }
    public void setAudioList(List<Audio> listado){
        /*metodo encargado de asignar la nueva lista que debe mostrar el recycler view
        es utilizado por el fragment principal para mandar la nueva lista que debe mostrar el recyclre view*/
        adapter.setListadoAudio(listado);
    }


    public RecyclerView_ListadoAudioAdapter getAdapter() {
        return adapter;
    }
    public void ocultarDeleteButton(){
        imageDelete.setVisibility(View.GONE);
    }
    public void mostrarDeleteButton(){
        imageDelete.setVisibility(View.VISIBLE);
    }

}
