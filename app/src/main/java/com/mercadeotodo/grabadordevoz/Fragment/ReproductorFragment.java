package com.mercadeotodo.grabadordevoz.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mercadeotodo.grabadordevoz.Clases.Audio;
import com.mercadeotodo.grabadordevoz.Clases.MyApplication;
import com.mercadeotodo.grabadordevoz.Interfaz.Comunicacion;
import com.mercadeotodo.grabadordevoz.R;
import com.triggertrap.seekarc.SeekArc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ReproductorFragment extends Fragment implements View.OnClickListener {
    private Comunicacion mListener;
    private Toolbar toolbar;
    private static Audio audio;
    private SeekArc seekBar;
    private static MediaPlayer player;
    private int forwardTime = 5000;
    private int backwardTime = 5000;
    private double startTime = 0;
    private double finalTime = 0;
    private Handler myHandler = new Handler();
    public static int oneTimeOnly = 0;
    private Button imgButtonplay;
    private Button imgButtonFoward;
    private Button imgButtonRewinds;
    private Button imgButtonStop;



    private TextView textViewDuration;
    private String newAudioFileName = "";
    private PrincipalFragment principalFragment;
    private Audio nuevoAudio;
    private String rutaParaNuevoArchivo = "";
    private List<MediaPlayer> listaDePlayers = new ArrayList<MediaPlayer>();
    private View view;

    public ReproductorFragment() {
        // Required empty public constructor
    }
    public static ReproductorFragment newInstance(Audio Audio) {
        ReproductorFragment fragment = new ReproductorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        audio = Audio;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        if (savedInstanceState != null) {
            /*si el savedInstance es distinto de nulo, significa que se salvo un audio
            antes de pausar el fragment, entonces lo rescatamos y se lo asignamos
            a la variable audio*/
            audio = (Audio) savedInstanceState.getParcelable("Audio");
        }
        this.setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().trackScreenView("Fragment Reproductor");
        try {
            /*obtenemos una referencia al principalFragment.
            y verificamos si hay players registrados en la lista encontrada ahi, y de haberlos
            le asignamos ese player al nuestro y verificamos si esta sonando*/
            getPrincipalFragment();
            listaDePlayers = principalFragment.getListaDePlayers();
            if (listaDePlayers.size() > 0){
                player = listaDePlayers.get(0);
                if (player.isPlaying()){
                /*si el reproductor esta sonando cuando regresamos, llamo al metodo play con la opcion 2
                para que actualice la interfaz grafica*/
                    play(2);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause","OnPause");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*salvamos la instancia del audio que estamos reproduciendo actualmente*/
        try {
            outState.putParcelable("Audio", audio);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_reproductor, container, false);
        inicializarComponentes(view);
         return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.reproductor_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_share:{
                shareAudio();
                break;
            }
            case R.id.action_rename:{
                showRenameSongDialog();
                break;
            }
            case R.id.action_copy:{
                mListener.openFilePicker(1, "Copy");
                break;
            }
            case R.id.action_move:{
                mListener.openFilePicker(2,"Move");
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            mListener = (Comunicacion) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean isPlayerPlaying(){
        /*retornamos la verificacion para ver si el player esta reproduciendo, para que de esta manera
        y a traves del fragmentPrincipal la actividad sepa si accionar el back button o ignorarlo
        y simplemente minimizar la app*/
        return player.isPlaying();
    }
    private void inicializarComponentes(View view) {
        imgButtonFoward = (Button) view.findViewById(R.id.imageFoward);
        imgButtonplay = (Button) view.findViewById(R.id.imageReproducir);
        imgButtonRewinds = (Button) view.findViewById(R.id.imageRewind);
        imgButtonStop = (Button) view.findViewById(R.id.imageParar);
        imgButtonStop.setOnClickListener(this);
        imgButtonRewinds.setOnClickListener(this);
        imgButtonFoward.setOnClickListener(this);
        imgButtonplay.setOnClickListener(this);
        imgButtonFoward.setEnabled(false);
        imgButtonRewinds.setEnabled(false);
        imgButtonStop.setEnabled(false);
        imgButtonplay.setEnabled(true);
        imgButtonplay.setTag(R.drawable.ic_action_play);
        toolbar = (Toolbar) view.findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        textViewDuration = (TextView) view.findViewById(R.id.fragmentReproductorTextReproductorDuration);
        seekBar = (SeekArc) view.findViewById(R.id.fragmentReproductorseekArc);



        try {
            if (audio != null) {
                toolbar.setTitle(audio.getNAME());
                AppCompatActivity activity = (AppCompatActivity) this.getActivity();
                activity.setSupportActionBar(toolbar);

                player =  MediaPlayer.create(getActivity(), Uri.parse(audio.getDATA()));
                finalTime = player.getDuration();
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        try {
                        /*cambiamos la imagen del imgButtonPlay, por la imagen play*/
                            imgButtonplay.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_action_play, 0, 0);
                            //imgButtonplay.setBackgroundDrawable((getResources().getDrawable(R.drawable.ic_action_play)));
            /*cambiamos el tag del imgButto, para asignarle la nueva imagen de play*/
                            imgButtonplay.setTag(R.drawable.ic_action_play);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        /*activamos el touch del seekbar, este metodo fue agregado por mi*/
                seekBar.setFlagTouch(true);
                seekBar.setProgress(0);
        /*asignamos el max del seekbar a la duracion de player*/
                seekBar.setMax(player.getDuration());
                seekBar.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
                    @Override
                    public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekArc seekArc) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekArc seekArc) {
                        // Log.e("PRUEBA SEEK BAR", " progress "+seekBar.getProgress());
                    /*adelantar el audio hasta donde avancen la seekbar*/
                        player.seekTo(seekBar.getProgress());
                    }
                });
            } else {
                /*si entra en una excepcion es pq el audio e snulo, por ende ponemos un mensaje y bloqueamos todos los botones*/
                toolbar.setTitle(getActivity().getString(R.string.NoArchivos));
                imgButtonplay.setEnabled(false);
                imgButtonFoward.setEnabled(false);
                imgButtonStop.setEnabled(false);
                imgButtonRewinds.setEnabled(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            /*si entra en una excepcion es pq el audio e snulo, por ende ponemos un mensaje y bloqueamos todos los botones*/
            toolbar.setTitle(getActivity().getString(R.string.NoArchivos));
            imgButtonplay.setEnabled(false);
            imgButtonFoward.setEnabled(false);
            imgButtonStop.setEnabled(false);
            imgButtonRewinds.setEnabled(false);
        }


    }


    public void stop(){
        player.pause();
        player.seekTo(0);
        /*cambiamos la imagen del imgButtonPlay, por la imagen play*/
        imgButtonplay.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_action_play, 0, 0);
        /*cambiamos el tag del imgButto, para asignarle la nueva imagen de play*/
        imgButtonplay.setTag(R.drawable.ic_action_play);
        imgButtonFoward.setEnabled(false);
        imgButtonRewinds.setEnabled(false);
        imgButtonStop.setEnabled(false);
        imgButtonplay.setEnabled(true);
    }
    public void limpiarListaDePlayers(){
         /*verificamos si existen otros Objetos Media player reproduciendose, obteniendo en primer lugar
         * una referencia del fragment principal y luego obteniendo una referencia a la lista de Players
         * instanciada ahi*/
        getPrincipalFragment();
        listaDePlayers = principalFragment.getListaDePlayers();
        if (listaDePlayers.size() > 0){
            for ( MediaPlayer playerClean : listaDePlayers) {
                /*si encontramos un objeto Media Player previamente almacenado y que se este reproduciendo,
                 le damos release para eliminarlo y  que deje de reproducir sonido*/
                if (playerClean != null && playerClean.isPlaying()) {
                    playerClean.release();
                }
            }
            /*limpiamos la lista, al terminar la busqueda de media players reproduciendo*/
            listaDePlayers.clear();
        }
        /*y agregamos este nuevo player a la lista*/
        listaDePlayers.add(player);
    }
    public void play(int opcion){
        if (opcion == 1) {
            limpiarListaDePlayers();
            player.start();
        }

        finalTime = player.getDuration();
        startTime = player.getCurrentPosition();
        if(oneTimeOnly == 0){
            oneTimeOnly = 1;
        }
        myHandler.postDelayed(UpdateSongTime, 100);
        imgButtonFoward.setEnabled(true);
        imgButtonRewinds.setEnabled(true);
        imgButtonStop.setEnabled(true);
        /*cambiamos la imagen del imgButton play, para asignarle la imagen de pause*/
        imgButtonplay.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_action_pause, 0, 0);
        /*almacenamos en el tag del imgButtonPlay, el nuevo valor de la imagen asignada, (pause)*/
        imgButtonplay.setTag(R.drawable.ic_action_pause);
        Log.e("play", "el player esta sonando " + player.isPlaying()+" el player is looping "+player.isLooping());

    }
    private String getTimeString(long millis) {
        /*metodo para darle formato al tiempo del audio*/
        StringBuffer buf = new StringBuffer();
        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);
        if (minutes > 60) {
            buf
                    .append(String.format("%02d", hours))
                    .append(":")
                    .append(String.format("%02d", minutes))
                    .append(":")
                    .append(String.format("%02d", seconds));
        } else {
            buf
                    .append(String.format("%02d", minutes))
                    .append(":")
                    .append(String.format("%02d", seconds));
        }

        return buf.toString();
    }


    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = player.getCurrentPosition();
            textViewDuration.setText(getTimeString(player.getCurrentPosition()));
            myHandler.postDelayed(this, 100);
            seekBar.setProgress(player.getCurrentPosition());
        }
    };
    public void pause(){
        player.pause();
        /*cambiamos la imagen del imgButtonPlay, por la imagen play*/
        imgButtonplay.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_action_play,0,0);
        /*cambiamos el tag del imgButto, para asignarle la nueva imagen de play*/
        imgButtonplay.setTag(R.drawable.ic_action_play);

    }
    public void forward(){
        int temp = (int)startTime;
        if((temp+forwardTime)<=finalTime){
            startTime = startTime + forwardTime;
            player.seekTo((int) startTime);
        }
    }
    public void rewind() {
        int temp = (int) startTime;
        if ((temp - backwardTime) > 0) {
            startTime = startTime - backwardTime;
            player.seekTo((int) startTime);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageFoward:{
                forward();
                break;
            }
            case R.id.imageParar:{
                stop();
                break;
            }
            case R.id.imageReproducir:{
                switch (imgButtonplay.getTag().hashCode()){
                    /*verificamos que imagen tiene en el momento de click el imgButton,
                    y de acuerdo a la imagen que poseea, llamamos a un metodo en especifico
                    este switch pasamos a int el object almacenado en el tag
                     */
                    case R.drawable.ic_action_play:{
                        play(1);
                        break;
                    }
                    case R.drawable.ic_action_pause:{
                        pause();
                        break;
                    }
                }
                break;
            }
            case R.id.imageRewind:{
                rewind();
                break;
            }
        }
    }

    public void setRutaParaNuevoArchivo(String rutaParaNuevoArchivo) {
        Log.e("SetRuta",rutaParaNuevoArchivo);
        this.rutaParaNuevoArchivo = rutaParaNuevoArchivo;
    }

    public void shareAudio(){
        /*compartir el audio por los distintos medios facebook, whatsapp, twitter, etc*/
        /*captura y transformacion de direccion del archivo*/
        String sharePath = audio.getDATA();
        // Uri uri = Uri.parse(sharePath);
        File fileIn = new File(sharePath);
        Uri uri = Uri.fromFile(fileIn);
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.setType("text/plain");
            emailIntent.setType("audio/*");
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Grabador de Voz Plus Audio mp3");
            emailIntent.putExtra(Intent.EXTRA_TEXT, getActivity().getString(R.string.Email_Body));

            getActivity().startActivity(Intent.createChooser(emailIntent, getActivity().getString(R.string.Share_Menu)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.Error_Shared), Toast.LENGTH_SHORT).show();
        }
    }
    public void renameSong(String path){
        Log.e("RENAME", path);
        /*creamos 2 archivos uno con la ruta del audio original y otro con la ruta del audio original
        pero hasta su carpeta padre es decir obviando el nombre y luego
        se le asigna el nuevo nombrecon el nuevo nombre*/
        File archivo = new File (path);
        File archivoRename = new File (archivo.getParent()+"/"+newAudioFileName+".mp3");
        /*oficialmente cambiamos el nombre del archivo (junto con su ruta) con el metodo renameTo*/
        archivo.renameTo(archivoRename);
        /*eliminamos del media store la informacion del archivo viejo, y actualizamos el media store con la nueva informacion*/
        eliminarAudioMediaStore(path);
        actualizarMediaStore(archivoRename.toString());
        /*creamos un objeto nuevoAudio para mandar al recycler view ambos datos para que busque en el listado y actualice la nueva informacion*/
        nuevoAudio = new Audio(
                audio.getID(),
                audio.getSIZE(),
                audio.getTITLE(),
                archivoRename.getAbsolutePath(),
                newAudioFileName + ".mp3",
                audio.getDURATION(),
                audio.getDATE(),
                audio.getSIZE_NUMERIC());
        Log.e("renameSong",audio.getDATA()+"   "+nuevoAudio.getDATA()+"  "+nuevoAudio.getNAME());
        getPrincipalFragment();
        if (principalFragment != null){
            principalFragment.editarAudio(audio,nuevoAudio);
        }

    }
    public void ejecutarAccion(String accion){
        Log.e("ejecutarAccion",accion);
        new MyAsynkTask(accion).execute();
    }
    public void setNuevoAudio(Audio audioNuevo){
        /*guardamos el nuevo audio recibido por parametro,
        llamamos a inicializar de nuevo los componentes para cargar la nueva informacion del audio,
        detenemos y limpiamos todos los player reproduciendose, y mandamos a reproducir el nuevo audio*/
        audio = audioNuevo;
        inicializarComponentes(view);
        play(1);
        Log.e("NuevoAudio",audio.getDATA());
    }
    private void CopyOrMoveSong (String filePath, String fileName, String opc) {
        /*inicializamos todos los componentes con los que trabajaremos*/
        File archivo;
        InputStream in = null;
        OutputStream out = null;
        /*variables locales para almacenar el nombre y la ruta del archivo "nuevo"*/
        String newFilePath = null;
        String nombre = "";
        try {
            /*intanciamos el objeto input el cual obtendra los datos del archivo original para el copiado o movida del mismo*/
            in = new FileInputStream(filePath);
            /*verificamos si se desea copiar o mover el archivo, para variar el nombre que se le colocara al nuevo archivo,
            en caso de copiar se le agrega la palabra copy al inicio del nombre, en el caso de movida, se mantiene el nombre original
             */
            if (opc.equals("Copy")) {
                out = new FileOutputStream(rutaParaNuevoArchivo+ "/"+getActivity().getString(R.string.AudioCopia)+"-" + fileName);
                archivo = new File(rutaParaNuevoArchivo+ "/"+getActivity().getString(R.string.AudioCopia)+"-" + fileName);
                nombre = getActivity().getString(R.string.AudioCopia)+"-" + fileName;
                newFilePath = rutaParaNuevoArchivo+ "/"+getActivity().getString(R.string.AudioCopia)+"-" + fileName;

            }
            else {
                out = new FileOutputStream(rutaParaNuevoArchivo+ "/" + fileName);
                archivo = new File(rutaParaNuevoArchivo+ "/" + fileName);
                nombre = fileName;
                newFilePath = rutaParaNuevoArchivo+ "/" + fileName;
            }

            /*se procede a copiar todos los bytes del archivo y a cerrar los respectos outputstream e inputstreams*/
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            /*creamos un nuevo objeto audio el cual tendra la informacion del "nuevo" archivo copiado o movido*/
            nuevoAudio = new Audio(
                    audio.getID(),
                    audio.getSIZE(),
                    audio.getTITLE(),
                    newFilePath,
                    nombre,
                    audio.getDURATION(),
                    audio.getDATE(),
                    audio.getSIZE_NUMERIC());

                /*obtenemos una referencia al fragment principal*/
                getPrincipalFragment();
                if (opc.equals("Move")){
                /*si estamos moviendo el archivo mandamos a eliminarlo*/
                    if (principalFragment != null){
                        principalFragment.eliminarAudio(audio);

                    }
                }
            /*actualizamos el media store y mandamos a agregar el "audio nuevo"*/
                actualizarMediaStore(nuevoAudio.getDATA());
                mListener.agregarAudio(nuevoAudio);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showRenameSongDialog(){
        /*metodo que muestra un alert dialog para cambiar el nombre
        del audio inicialmente se instancia un AlertDialog.Builder y un Edit Text*/
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setTitle(getActivity().getString(R.string.Title_Rename_Song));
        final EditText input = new EditText(getActivity());
        input.setSingleLine(true);

        /*se configura el alertDialogBuilder*/
        alertBuilder.setView(input);
        alertBuilder.setPositiveButton(getActivity().getString(R.string.Save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                newAudioFileName = input.getText().toString().trim();
                toolbar.setTitle(newAudioFileName+".mp3");
                renameSong(audio.getDATA());
            }
        });

        alertBuilder.setNegativeButton(getActivity().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        /*se instancia ahora un AlertDialog partiendo del AlertDialogBuilder*/
        final AlertDialog dialog = alertBuilder.create();
        dialog.show();
        /*se obtiene el boton aceptar del dialog y se desactiva*/
        dialog.getButton(AlertDialog.BUTTON1).setEnabled(false);
        /*a traves del metodo on textChangeListener, desactivamos y activamos
        el boton "Ok" cuando este el edit text con letras*/
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*setEnable, depende del escrito en el editText sin espacios y verificando que no este vacio
                como devuelve true si se encuentra vacio, se niega para que devuelva false y deshabilitar
                el boton "Ok" del dialog*/
                dialog.getButton(AlertDialog.BUTTON1).setEnabled(!s.toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private void actualizarMediaStore(String audioNuevo){
        /*escaneo para actualizar el media store y asi visualizar la nueva grabacion realizada*/
        MediaScannerConnection.scanFile(getActivity(),
                new String[]{audioNuevo}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String audioNuevo, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + audioNuevo + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
    }
    private void eliminarAudioMediaStore(String audioOriginal){
        MediaScannerConnection.scanFile(getActivity(), new String[]{audioOriginal},
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String audioOriginal, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + audioOriginal + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                        getActivity().getContentResolver()
                                .delete(uri, null, null);
                    }
                });
    }
    private void getPrincipalFragment(){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        principalFragment = (PrincipalFragment) manager.findFragmentByTag("Principal");

    }
    private class MyAsynkTask extends AsyncTask<String, Void, Boolean> {
        String opcion;

        public MyAsynkTask(String opcion) {
            this.opcion = opcion;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if (  (opcion.equals("Copy")) || (opcion.equals("Move") && (!audio.getDATA().equals(rutaParaNuevoArchivo+ "/" + audio.getNAME()))) ) {
                /*validamos que si se desea mover el archivo, no sea para la misma ruta en la que se encuentra*/
                CopyOrMoveSong(audio.getDATA(),audio.getNAME(),opcion);
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
             /*llamamos a notifyDataSetchanged para actualizar el recycler view*/
            switch (opcion){
                case "Copy":{
                    Toast.makeText(getActivity(),getActivity().getString(R.string.File_Copied),Toast.LENGTH_SHORT).show();

                    break;
                }
                case "Move":{
                    Toast.makeText(getActivity(),getActivity().getString(R.string.File_Moved),Toast.LENGTH_SHORT).show();
                    break;
                }
            }

        }
    }
}
