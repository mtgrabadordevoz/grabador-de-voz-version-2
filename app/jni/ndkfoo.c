#include <string.h>
#include <jni.h>

jstring Java_com_mercadeotodo_grabadordevoz_Actividad_MainActivity_invokeNativeFunction(JNIEnv* env, jobject javaThis) {
  return (*env)->NewStringUTF(env, "Hello from native code!");
}